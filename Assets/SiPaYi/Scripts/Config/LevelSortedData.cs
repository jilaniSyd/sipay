namespace KnightHawkStudios.Sipayi
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class LevelSortedData : ScriptableObject
    {
        public List<LevelSortedInfo> Data = new List<LevelSortedInfo>();
    }

    [Serializable]
    public class LevelSortedInfo
    {
        public LevelDifficulty _levelDifficulty;
        public List<LevelBlock> _levelCollection;

        public LevelSortedInfo(LevelDifficulty levelDifficulty, List<LevelBlock> levelBlocks)
        {
            _levelDifficulty = levelDifficulty;
            _levelCollection = levelBlocks;
        }
    }
}