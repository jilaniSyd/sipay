﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "DialogIconsConfig", menuName = "SiPaYi/Config", order = 2)]
    public class DialogIconsConfig : ScriptableObject
    {
        public List<DialogIcons> Icons;
    }

    [Serializable]
    public class DialogIcons
    {
        public CharacterType characterType;
        public Sprite Sprite;
    }
}