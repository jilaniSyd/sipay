﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CreateAssetMenu(fileName = "LevelConfig", menuName = "SiPaYi/LevelConfig", order = 1)]
    public class LevelConfig : ScriptableObject
    {
        /// <summary>
        /// Number of blocks loads for each sequence
        /// </summary>
        public int LoadSize;

        /// <summary>
        /// Defines how many levelblocks should be left to start loading new ones
        /// </summary>
        public int LoadOffset;

        public List<LevelDifficulty> LevelDifficulties;
    }
}