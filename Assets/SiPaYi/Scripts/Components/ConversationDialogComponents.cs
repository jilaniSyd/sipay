﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

[Game, Unique]
public sealed class ConversationDialogBalancingDataComponent : IComponent
{
    public Dictionary<string, ConversationData> conversationData;
}

public sealed class ConversationData
{
    public List<ConversationSequenceData> dialogs = new List<ConversationSequenceData>();
}

public sealed class ConversationSequenceData
{
    public string Id;
    public CharacterType characterType;
    public string text;

    public ConversationSequenceData(DialogsData data)
    {
        Id = data.Dialogid;
        characterType = data.CHARACTERTYPE;
        text = data.Dialog;
    }
}

[Game]
public sealed class ConversationDialogShowComponent : IComponent
{
    public string SequenceId;
}

[Game, Unique]
public sealed class ConversationDialogComponent : IComponent
{
}

[Game]
public sealed class ConversationDialogFinishComponent : IComponent
{
}

[Game]
public sealed class ConversationSequenceIndexComponent : IComponent
{
    public int Index;
}

[Game]
public sealed class ConversationSequenceDataComponent : IComponent
{
    public List<ConversationSequenceData> Data;
}

[Game]
public sealed class ConversationSequenceShowComponent : IComponent
{
    public string dialogText;
    public UnityEngine.Sprite characterSprite;
}

[Game]
public sealed class ConversationSequenceHideComponent : IComponent
{
}