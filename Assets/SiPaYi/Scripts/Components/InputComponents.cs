﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Input]
public sealed class PlayerJump : IComponent { }

[Input]
public sealed class PlayerDefend : IComponent { }

[Input]
public sealed class PlayerAttack : IComponent { }

[Game, Input]
public sealed class DestroyedComponent : IComponent { }

[Game, Event(EventTarget.Any)]
public sealed class WeaponSelectedComponent : IComponent
{
    public int Id;
}

[Game, Event(EventTarget.Any)]
public sealed class CharacterSelectedComponent : IComponent
{
    public int Id;
}

[Game]
public sealed class GameRestart : IComponent
{
    
}