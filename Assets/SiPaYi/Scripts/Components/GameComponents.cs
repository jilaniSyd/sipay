﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using KnightHawkStudios.Sipayi;
using System;
using System.Collections.Generic;

[Game]
public sealed class ScreenShowComponent : IComponent
{
    public Type Screen;
}

[Input]
public sealed class InputComponent : IComponent { }

[Game, Unique]
public sealed class ScreenStackComponent : IComponent
{
    public Stack<BaseScreen> Stack;
}

[Game]
public sealed class ScreenComponent : IComponent { }

[Game]
public sealed class ScreenCloseComponent : IComponent
{
    public Type SceneName;
}

[Game]
public sealed class SceneLoadedComponent : IComponent
{
    public string SceneName;
}

[Game]
public sealed class PopupShowComponent : IComponent
{
    public Type Popup;
}

[Game]
public sealed class PopupHideComponent : IComponent
{
    public Type Popup;
}

[Game]
public sealed class PopupComponent : IComponent { }

[Game]
public sealed class CameraMoveComponent : IComponent { }

[Game]
public sealed class LoadBlocksComponent : IComponent { }

[Game]
public sealed class UnloadBlockComponent : IComponent
{
    public LevelBlock levelBlock;
}

[Game, Unique]
public sealed class LevelDataComponent : IComponent { }

[Game]
public sealed class LevelBlocksComponent : IComponent
{
    public List<LevelBlock> Blocks;
}

[Game]
public sealed class LevelBlockIndex : IComponent
{
    public int Index;
}

[Game]
public sealed class LevelConfigComponent : IComponent
{
    public LevelConfig Config;
}

[Game]
public sealed class LevelSortedDataComponent : IComponent
{
    public LevelSortedData SortedData;
}

[Game]
public sealed class LevelSpawnPointComponent : IComponent
{
    public float xOffset;
}

[Game]
public sealed class BlockEndReachedComponent : IComponent
{
}

[Game]
public sealed class BlockStartReachedComponent : IComponent
{
    public LevelBlock levelBlock;
}

[Game]
public sealed class PlayerInBlockComponent : IComponent
{
    public LevelBlock levelBlock;
}

[Game]
public sealed class PlayerNextBlockComponent : IComponent
{
    public LevelBlock levelBlock;
}

[Game]
public sealed class PlayerInBlockIndexComponent : IComponent
{
    public int Index;
}

[Game, Unique]
public sealed class CharacterBalancingData : IComponent
{
    public Dictionary<int, CharactersData> Data;
}

[Game, Unique]
public sealed class WeaponsBalancingData : IComponent
{
    public Dictionary<int, WeaponsData> Data;
}

[Game, Unique]
public sealed class WeaponSelectedId : IComponent
{
    public int Id;
}

[Game, Unique]
public sealed class CharacterSelectedId : IComponent
{
    public int Id;
}

[Game, Unique]
public sealed class PlayerHighScoreComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class SavePlayerScoreComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class SavePlayerCoinsComponent : IComponent
{
    public int Amount;
}

[Game, Unique]
public sealed class PlayerEpicScoreComponent : IComponent
{
    public int Amount;
}

[Game, Unique]
public sealed class PlayerCoinsComponent : IComponent
{
    public int Amount;
}

[Game, Unique]
public sealed class PlayerHeartsComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class ActionEventComponent : IComponent
{
    public Action action;
}