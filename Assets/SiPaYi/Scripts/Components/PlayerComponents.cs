﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using KnightHawkStudios.Sipayi;
using UnityEngine;

[Game]
public sealed class PlayerControllerComponent : IComponent
{
    public PlayerController Controller;
}

[Game, Unique]
public sealed class PlayerComponent : IComponent { }

[Game]
public sealed class HealthComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class LivesComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class CoinsComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class Rigidbody2DComponent : IComponent
{
    public UnityEngine.Rigidbody2D rigidbody2D;
}

[Game]
public sealed class TransformComponent : IComponent
{
    public UnityEngine.Transform transform;
}

[Game]
public sealed class GroundedComponent : IComponent
{
    public bool Grounded;
}

[Game]
public sealed class DeadComponent : IComponent { }

[Game]
public sealed class AboutToDieComponent : IComponent { }

[Game]
public sealed class CoinCollectedComponent : IComponent { }

[Game, Event(EventTarget.Any)]
public sealed class CoinUpdateComponent : IComponent
{
    public int Amount;
}

[Game, Event(EventTarget.Any)]
public sealed class HeartUpdateComponent : IComponent
{
    public int Amount;
}

[Game]
public sealed class HealthPortionCollectedComponent : IComponent { }

[Game]
public sealed class HealthPortionLostComponent : IComponent { }

[Game]
public sealed class PlayerJumpComponent : IComponent { }

[Game]
public sealed class PlayerAttackComponent : IComponent { }

[Game]
public sealed class PlayerDefendComponent : IComponent { }

[Game]
public sealed class PlayerTargetPositionComponent : IComponent
{
    public Vector3 position;
}