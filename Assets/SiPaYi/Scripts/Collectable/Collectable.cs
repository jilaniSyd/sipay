﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class Collectable : MonoBehaviour, ICollectable
    {
        public virtual void OnCollected(PlayerController playerController)
        {
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                Debug.Log("Collided with " + other.name);
                var playerController = other.transform.GetComponent<PlayerController>();
                OnCollected(playerController);
                Destroy(gameObject);
            }
        }
    }
}