namespace KnightHawkStudios.Sipayi
{
    public class HealthPortionCollectable : Collectable
    {
        public override void OnCollected(PlayerController playerController)
        {
            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.isHealthPortionCollected = true;
        }
    }
}