﻿namespace KnightHawkStudios.Sipayi
{
    public class CoinCollectable : Collectable
    {
        public override void OnCollected(PlayerController playerController)
        {
            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.isCoinCollected = true;
        }
    }
}