namespace KnightHawkStudios.Sipayi
{
    internal interface ICollectable
    {
        void OnCollected(PlayerController PlayerController);
    }
}