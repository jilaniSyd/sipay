﻿public enum ObstacleType
{
    Falling,
    Flying,
    Trap,
}

public enum ScreenID
{
    CharacterSelection,
    WeaponSelection,
    ResultScreen,
    SettingsPopup,
    MainMenuScreen,
    InGameHud,
    DialogScreen
}

public enum CharacterType
{
    Sipayi,
    King,
    Seer,
}