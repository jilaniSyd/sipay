﻿namespace KnightHawkStudios.Sipayi
{
    public enum LevelElementType
    {
        Moving,
        Static,
    }

    public enum LevelDifficulty
    {
        EASY,
        MEDIUM,
        HARD,
        MEDIUM2,
        HARD2
    }
}