﻿namespace KnightHawkStudios.Sipayi
{
    public static class Constants
    {
        public const string TAG_PLAYER = "Player";
        public const string TAG_GROUND = "Ground";
        public const string TAG_LEVEL_BLOCK_END = "LevelBlockEnd";
        public const string TAG_LEVEL_BLOCK_START = "LevelBlockStart";
        public const string TAG_OBSTACLE = "Obstacle";
        public const string TAG_OBSTACLE_TRAP = "Trap";
        public const string TAG_BIRD = "Bird";
        public const string TAG_BIRD_EGG = "BirdEgg";

        public const string CHARACTER_ID = "CharacterId";
        public const string WEAPON_ID = "WeaponId";
        public const string PLAYER_COINS = "Coins";
        public const string PLAYER_HEARTS = "Hearts";
        public const string PLAYER_HIGHSCORE = "HighScore";
    }
}