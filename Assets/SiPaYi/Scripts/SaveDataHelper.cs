﻿namespace KnightHawkStudios.Sipayi
{
    public static class SaveDataHelper
    {
        public static int GetCharacterId()
        {
            return Contexts.sharedInstance.game.characterSelectedId.Id;
        }

        public static int GetWeaponId()
        {
            return Contexts.sharedInstance.game.weaponSelectedId.Id;
        }

        public static int GetHighScore()
        {
            int highScoreAmount = 0;
            if (Contexts.sharedInstance.game.hasPlayerHighScore)
            {
                highScoreAmount = Contexts.sharedInstance.game.playerHighScore.Amount;
            }
            return highScoreAmount;
        }

        public static int GetCoins()
        {
            int coins = 0;

            if (Contexts.sharedInstance.game.hasPlayerCoins)
            {
                coins = Contexts.sharedInstance.game.playerCoins.Amount;
            }
            return coins;
        }

        public static int GetEpicScore()
        {
            int epicScore = 0;

            if (Contexts.sharedInstance.game.hasPlayerEpicScore)
            {
                epicScore = Contexts.sharedInstance.game.playerEpicScore.Amount;
            }
            return epicScore;
        }
    }
}