﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class LevelBlockEnd : MonoBehaviour
    {
        private bool _triggered = false;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                if (_triggered) { return; }
                _triggered = true;

                Contexts.sharedInstance.game.CreateEntity().isBlockEndReached = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                _triggered = false;
            }
        }
    }
}