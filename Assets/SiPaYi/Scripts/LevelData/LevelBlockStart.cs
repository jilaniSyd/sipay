﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class LevelBlockStart : MonoBehaviour
    {
        private bool _triggered = false;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                if (_triggered) { return; }
                _triggered = true;

                var levelBlock = gameObject.GetComponentInParent<LevelBlock>();
                Contexts.sharedInstance.game.CreateEntity().AddBlockStartReached(levelBlock);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                _triggered = false;
            }
        }
    }
}