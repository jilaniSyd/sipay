﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    [ExecuteInEditMode]
    public class LevelElementsSnapper : MonoBehaviour
    {
        [SerializeField]
        private float snapStartPoint = 0;

        [ContextMenu("Snap")]
        public void Snap()
        {
            var childrens = transform.GetComponentsInChildren<Obstacle>(true);
            var index = 0;
            foreach (var child in childrens)
            {
                var childObstacle = child is MovingObstacle;
                if (childObstacle) { continue; }

                child.transform.position = new Vector3(snapStartPoint + index, child.transform.position.y, child.transform.position.z);
                child.transform.SetSiblingIndex(index);
                index++;
            }
        }
    }
}