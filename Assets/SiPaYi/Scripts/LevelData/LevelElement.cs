﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using UnityEngine;

    [Serializable]
    public class LevelElement : MonoBehaviour
    {
        public Vector2 position;
        public LevelElementType type;
    }
}