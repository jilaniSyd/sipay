﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class LevelData
    {
        public List<LevelElement> levelElements;
    }
}