﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using UnityEngine;

    public class LevelBlock : MonoBehaviour
    {
        public string BlockName { get { return blockName; } }

        [SerializeField]
        private string blockName;

        public LevelDifficulty difficulty;

        public float Length;

        public List<Obstacle> obstacles;
    }
}