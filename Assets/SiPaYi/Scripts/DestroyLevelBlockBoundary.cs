﻿namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using UnityEngine;

    public class DestroyLevelBlockBoundary : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_LEVEL_BLOCK_END)
            {
                Debug.Log("Unload level");
                var levelBlock = other.transform.GetComponentInParent(typeof(LevelBlock)) as LevelBlock;
                Contexts.sharedInstance.game.CreateEntity().AddUnloadBlock(levelBlock);
            }

            //May be should be returned to pool ?
            if (other.tag == Constants.TAG_BIRD)
            {
                Destroy(other.gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
        }
    }
}