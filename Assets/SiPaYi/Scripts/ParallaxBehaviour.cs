﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class ParallaxBehaviour : MonoBehaviour
    {
        [SerializeField]
        private float parallaxSpeed;

        [SerializeField]
        private GameObject cam;

        private float length;
        private float startPos;

        // Start is called before the first frame update
        private void Start()
        {
            startPos = transform.position.x;
            length = GetComponent<SpriteRenderer>().bounds.size.x;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            float temp = (cam.transform.position.x * (1 - parallaxSpeed));
            var dist = (cam.transform.position.x * parallaxSpeed);

            transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);

            if (temp > startPos + length)
            {
                startPos += length;
            }
            else if (temp < startPos - length)
            {
                startPos -= length;
            }
        }
    }
}