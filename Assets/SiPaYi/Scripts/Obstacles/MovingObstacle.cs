namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    public class MovingObstacle : Obstacle
    {
        [SerializeField] private Vector2 direction;
        [SerializeField] private bool autoDestroyOnCollision;
        [SerializeField] private float movementDuration;

        private Rigidbody2D rgbody2d;

        private void Awake()
        {
            rgbody2d = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            direction = transform.position + (Vector3)direction;
            Move();
        }

        private void Move()
        {
            transform.DOMove(direction, movementDuration, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InFlash).SetUpdate(true);
        }
    }
}