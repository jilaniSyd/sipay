﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections;
    using UnityEngine;

    public class TrapObstacleBehavior : GroundObstacle
    {
        [SerializeField]
        private float waitTimeToShow = 1.5f;

        [SerializeField]
        private float waitTimeToOpen = 2f;

        [SerializeField]
        private float waitTimeToClose = 1f;

        private string trapAnimationStartFlag = "TrapStart";
        private string trapAnimationCloseFlag = "TrapClose";
        private string trapAnimationOpenFlag = "TrapShow";

        private Animator animator;

        private void Start()
        {
            animator = GetComponent<Animator>();
            StartCoroutine(StartAnimation());
        }

        private IEnumerator StartAnimation()
        {
            yield return new WaitForSeconds(waitTimeToShow);
            animator.SetTrigger(trapAnimationStartFlag);
            yield return new WaitForSeconds(waitTimeToOpen);
            animator.SetTrigger(trapAnimationOpenFlag);
            yield return new WaitForSeconds(waitTimeToClose);
            animator.SetTrigger(trapAnimationCloseFlag);
            StartCoroutine(StartAnimation());
        }
    }
}