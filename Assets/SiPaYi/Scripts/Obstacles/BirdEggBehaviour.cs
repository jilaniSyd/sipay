﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class BirdEggBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform EggBreakFx;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == Constants.TAG_PLAYER)
            {
                Debug.Log(GetType().Name + "Triggered with " + collision.name);
                Destroy(gameObject);
                PlayBreakFx();
            }

            if (collision.tag == Constants.TAG_GROUND)
            {
                Debug.Log(GetType().Name + "Triggered with " + collision.name);
                Destroy(gameObject);
                PlayBreakFx();
            }
        }

        private void PlayBreakFx()
        {
            Instantiate(EggBreakFx, transform.position, Quaternion.identity);
        }
    }
}