namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    public class FallingObstacle : GroundObstacle
    {
        private Rigidbody2D rgbody2d;

        private void Awake()
        {
            rgbody2d = GetComponent<Rigidbody2D>();
        }

        public override void OnCollisionWithObstacle(Collision2D other)
        {
            if (other.transform.tag == Constants.TAG_PLAYER)
            {
                DOVirtual.DelayedCall(0.2f, Fall);
            }
        }

        private void Fall()
        {
            rgbody2d.bodyType = RigidbodyType2D.Dynamic;
            rgbody2d.constraints = RigidbodyConstraints2D.FreezeRotation;
            rgbody2d.gravityScale = 2f;
        }
    }
}