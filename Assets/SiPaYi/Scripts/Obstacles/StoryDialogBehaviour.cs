﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class StoryDialogBehaviour : MonoBehaviour
    {
        [SerializeField]
        private string sequenceId;

        private bool isDialogShown = false;

        private void OnEnable()
        {
            isDialogShown = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == Constants.TAG_PLAYER && !isDialogShown)
            {
                isDialogShown = true;
                Debug.Log("On Story Dialog");
                var conversationEntity = Contexts.sharedInstance.game.CreateEntity();
                conversationEntity.AddConversationDialogShow(sequenceId);
            }
        }
    }
}