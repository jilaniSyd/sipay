﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class BirdBehaviour : MonoBehaviour
    {
        [SerializeField] private float birdFlyDistanceToPlayer = 3f;
        [SerializeField] private float birdAttackDistanceToPlayer = 3f;
        [SerializeField] private float speedOfBirdFly = 4f;
        [SerializeField] private float eggDropDistanceFromPlayer = 0.5f;
        [SerializeField] private Transform eggDropPrefab;

        private GameEntity playerEntity;
        private Vector3 playerPosition;
        private bool isBirdStartedFlying = false;
        private bool eggDropped = false;

        private void Start()
        {
            if (Contexts.sharedInstance.game.isPlayer)
            {
                playerEntity = Contexts.sharedInstance.game.playerEntity;
            }
        }

        private void Update()
        {
            if (playerEntity != null && !playerEntity.isDead)
            {
                if (playerEntity.hasTransform)
                {
                    playerPosition = playerEntity.transform.transform.position;
                    if (!isBirdStartedFlying && transform.position.x - playerPosition.x < birdFlyDistanceToPlayer)
                    {
                        isBirdStartedFlying = true;
                    }
                }
            }

            if (isBirdStartedFlying)
            {
                FlyBird();
            }
        }

        private void FlyBird()
        {
            transform.Translate(new Vector3(-1f, 0f, 0f) * speedOfBirdFly * Time.smoothDeltaTime);
            if (!eggDropped && transform.position.x - playerPosition.x < eggDropDistanceFromPlayer)
            {
                eggDropped = true;
                DropEgg();
            }
        }

        private void DropEgg()
        {
            var egg = Instantiate(eggDropPrefab, transform.position, Quaternion.identity);
        }
    }
}