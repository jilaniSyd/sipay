﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class Obstacle : MonoBehaviour, IObstacle
    {
        public virtual void OnCollisionWithObstacle(Collision2D other)
        {
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            OnCollisionWithObstacle(other);
        }
    }
}