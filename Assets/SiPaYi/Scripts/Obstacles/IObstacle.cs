using UnityEngine;

namespace KnightHawkStudios.Sipayi
{
    internal interface IObstacle
    {
        void OnCollisionWithObstacle(Collision2D other);
    }
}