﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using UnityEngine;

    public class WeaponsListDisplayItem : MonoBehaviour
    {
        [SerializeField] private GameObject weaponItemPrefab;

        private List<WeaponDisplayItem> weaponsItems = new List<WeaponDisplayItem>();

        public List<WeaponDisplayItem> Setup(List<WeaponsData> items)
        {
            foreach (var item in items)
            {
                var newWeapon = Instantiate(weaponItemPrefab, transform, false);
                var weaponDisplayItem = newWeapon.GetComponent<WeaponDisplayItem>();
                weaponDisplayItem.SetView(item);
                weaponsItems.Add(weaponDisplayItem);
            }
            return weaponsItems;
        }
    }
}