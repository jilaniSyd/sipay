﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using UnityEngine;

    public class CharacterListDisplayItem : MonoBehaviour
    {
        [SerializeField] private GameObject characterItemPrefab;

        private List<CharacterDisplayItem> weaponsItems = new List<CharacterDisplayItem>();

        //TODO : No Pooling used for now
        public List<CharacterDisplayItem> Setup(List<CharactersData> items)
        {
            foreach (var item in items)
            {
                var newWeapon = Instantiate(characterItemPrefab, transform, false);
                var weaponDisplayItem = newWeapon.GetComponent<CharacterDisplayItem>();
                weaponDisplayItem.SetView(item);
                weaponsItems.Add(weaponDisplayItem);
            }
            return weaponsItems;
        }
    }
}