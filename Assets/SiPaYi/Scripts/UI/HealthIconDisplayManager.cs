﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HealthIconDisplayManager : MonoBehaviour, IHeartUpdateListener
{
    [SerializeField]
    private GameObject heartIconPrefab;

    private List<GameObject> heartIconList = new List<GameObject>();

    private GameEntity listenerEntity;

    private void Awake()
    {
        foreach (var item in heartIconList)
        {
            Destroy(item);
        }
    }

    private void OnEnable()
    {
        listenerEntity = Contexts.sharedInstance.game.CreateEntity();
        listenerEntity.AddHeartUpdateListener(this);
    }

    private void OnDisable()
    {
        if (listenerEntity != null)
        {
            listenerEntity.Destroy();
        }
    }

    private void Start()
    {
        var playerEntity = Contexts.sharedInstance.game.playerEntity;
        var totalLives = playerEntity.lives.Amount;
        for (int i = 0; i < totalLives; i++)
        {
            heartIconList.Add(GetNewHeartIcon());
        }
    }

    private GameObject GetNewHeartIcon()
    {
        var newItem = Instantiate(heartIconPrefab, transform, false);
        newItem.SetActive(true);
        return newItem;
    }

    public void OnHeartUpdate(GameEntity entity, int Amount)
    {
        if (heartIconList.Count < Amount)
        {
            var diff = Amount - heartIconList.Count;
            for (int i = 0; i < diff; i++)
            {
                heartIconList.Add(GetNewHeartIcon());
            }
        }
        else if (heartIconList.Count > Amount)
        {
            var diff = heartIconList.Count - Amount;
            var iconsToDelete = heartIconList.TakeLast(diff);
            foreach (var item in iconsToDelete)
            {
                Destroy(item);
                heartIconList.Remove(item);
            }
        }
    }
}