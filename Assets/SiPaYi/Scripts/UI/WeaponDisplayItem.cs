﻿namespace KnightHawkStudios.Sipayi
{
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class WeaponDisplayItem : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI itemNameText;
        [SerializeField] private Button itemButton;
        [SerializeField] private Image bgImage;
        public int Id;

        private void OnEnable()
        {
            itemButton.onClick.AddListener(OnSelected);
        }

        private void OnDisable()
        {
            itemButton.onClick.RemoveListener(OnSelected);
        }

        public void SetView(WeaponsData item)
        {
            //set Icon
            itemNameText.text = item.Name;
            Id = System.Convert.ToInt32(item.Id);
        }

        private void OnSelected()
        {
            Select();

            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.AddWeaponSelected(Id);
        }

        public void SetView(CharactersData item)
        {
            //set Icon
            itemNameText.text = item.Name;
            Id = System.Convert.ToInt32(item.Id);
        }

        public void Select()
        {
            bgImage.color = Color.blue;
        }

        public void UnSelect()
        {
            bgImage.color = Color.white;
        }
    }
}