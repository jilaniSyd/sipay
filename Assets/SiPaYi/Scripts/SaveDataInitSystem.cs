﻿using Entitas;
using UnityEngine;
using KnightHawkStudios.Sipayi;

public class SaveDataInitSystem : IInitializeSystem
{
    public readonly GameContext gameContext;

    public SaveDataInitSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        if (!gameContext.hasCharacterSelectedId)
        {
            var characterSelectedId = PlayerPrefs.GetInt(Constants.CHARACTER_ID, 0);
            gameContext.SetCharacterSelectedId(characterSelectedId);
        }

        if (!gameContext.hasWeaponSelectedId)
        {
            var weaponSelectedId = PlayerPrefs.GetInt(Constants.WEAPON_ID, 0);
            gameContext.SetWeaponSelectedId(weaponSelectedId);
        }

        if (!gameContext.hasPlayerHighScore)
        {
            var highScore = PlayerPrefs.GetInt(Constants.PLAYER_HIGHSCORE, 0);
            gameContext.ReplacePlayerHighScore(highScore);
        }

        if (!gameContext.hasPlayerCoins)
        {
            var coins = PlayerPrefs.GetInt(Constants.PLAYER_COINS, 0);
            gameContext.ReplacePlayerCoins(coins);
        }
    }
}