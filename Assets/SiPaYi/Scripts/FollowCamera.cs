﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class FollowCamera : MonoBehaviour
    {
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                Follow();
            }
        }

        private void Follow()
        {
            Contexts.sharedInstance.game.CreateEntity().isCameraMove = true;
        }
    }
}