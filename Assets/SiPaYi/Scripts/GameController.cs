﻿using Entitas;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private Systems _systems;

    private void Awake()
    {
        var contexts = Contexts.sharedInstance;
        _systems = new GameSystems(contexts);
    }

    private void Start()
    {
        _systems.Initialize();
    }

    private void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }

    private void OnDestroy()
    {
        _systems.TearDown();
    }
}