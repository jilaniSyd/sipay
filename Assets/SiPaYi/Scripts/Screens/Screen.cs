namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public abstract class Screen : MonoBehaviour
    {
        public virtual void Hide(){}

        public virtual void Show(){}
    }
}