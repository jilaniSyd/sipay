﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using System.Linq;
    using TMPro;
    using UnityEngine;

    public class CharacterSelectionScreen : BaseScreen, ICharacterSelectedListener
    {
        [SerializeField] private TextMeshProUGUI characterNameText;
        [SerializeField] private TextMeshProUGUI characterDetailsText;
        [SerializeField] private CharacterListDisplayItem characterListPrefab;
        [SerializeField] private Transform displayItemsParent;
        [SerializeField] private DialogScreenBanner banner;

        private GameEntity characterSelectListenerEntity;
        private int selectedId = -1;
        private Dictionary<int, CharactersData> data;

        private List<CharacterDisplayItem> characterDisplayList = new List<CharacterDisplayItem>();

        public override ScreenID ScreenId { get { return ScreenID.CharacterSelection; } }

        private void OnEnable()
        {
            characterSelectListenerEntity = Contexts.sharedInstance.game.CreateEntity();
            characterSelectListenerEntity.AddCharacterSelectedListener(this);

            //TODO fetch the initial id From save data
            SetSelectedItem(SaveDataHelper.GetCharacterId());
            banner.SetView(this, "CHARACTERS", "");
        }

        private void OnDisable()
        {
            if (characterSelectListenerEntity != null)
            {
                characterSelectListenerEntity.Destroy();
            }
        }

        private void Start()
        {
            data = Contexts.sharedInstance.game.characterBalancingData.Data;
            SetupView(data.Values.ToList());
            SetSelectedItem(SaveDataHelper.GetWeaponId());
        }

        private void OnBackButton()
        {
            //pop from the stack?
            Debug.Log("Back Button");
        }

        #region Input Handling

        private void OnSettingsButton()
        {
            //Show Settings Popup
        }

        private void OnRightButton()
        {
            //Show Ads---Check the flow
            ScreenHelper.OpenScreen(typeof(WeaponSelectionScreen));
        }

        private void OnLeftButton()
        {
            ScreenHelper.CloseScreen(this.GetType());
        }

        private void OnMiddleButton()
        {
            //Make Selection and go to
            ScreenHelper.PlayGame();
        }

        #endregion Input Handling

        //TODO Could be done in a better way. This is the dirty way
        private void SetupView(List<CharactersData> charactersList)
        {
            var weaponsListCount = charactersList.Count / 3;
            int index = 0;
            for (int i = 0; i < weaponsListCount; i++)
            {
                var itemsList = charactersList.GetRange(index, 3);
                var item = CreateDisplayList(itemsList);
                characterDisplayList.AddRange(item);
                index += 3;
            }
            var remainingItems = charactersList.Count % 3;
            if (remainingItems > 0)
            {
                var lastWeaponsList = charactersList.GetRange(index, remainingItems);
                var lastItems = CreateDisplayList(lastWeaponsList);
                characterDisplayList.AddRange(lastItems);
            }
        }

        private List<CharacterDisplayItem> CreateDisplayList(List<CharactersData> characters)
        {
            var item = Instantiate(characterListPrefab, displayItemsParent, false);
            return item.Setup(characters);
        }

        public void OnCharacterSelected(GameEntity entity, int Id)
        {
            if (!selectedId.Equals(Id.ToString()))
            {
                var previousSelectedItem = characterDisplayList.Find(x => x.Id.Equals(selectedId));
                if (previousSelectedItem != null)
                {
                    previousSelectedItem.UnSelect();
                }
                selectedId = Id;
                SetSelectedItem(Id);
            }
        }

        private void SetSelectedItem(int selectedId)
        {
            if (selectedId == -1)
            {
                characterNameText.text = "";
                characterDetailsText.text = "";
                return;
            }
            if (data == null) { return; }

            var character = data[selectedId];
            if (character != null)
            {
                characterNameText.text = character.Name.ToUpper();
                characterDetailsText.text = string.Format("character ability {0}", character.Stamina);
            }
            else
            {
                characterNameText.text = "";
                characterDetailsText.text = "";
            }

            UnSelectAllAndSetSelected(selectedId);
        }

        private void UnSelectAllAndSetSelected(int selectedId)
        {
            foreach (CharacterDisplayItem item in characterDisplayList)
            {
                if (item.Id == selectedId)
                {
                    item.Select();
                }
                else
                {
                    item.UnSelect();
                }
            }
        }
    }
}