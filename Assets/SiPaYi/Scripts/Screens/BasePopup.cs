﻿namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using UnityEngine;

    public class BasePopup : Screen
    {
        [Header("Base Properties")]
        [SerializeField]
        private RectTransform popupTransform;

        [SerializeField]
        private CanvasGroup CanvasGroup;

        public string ScreenId { get; set; }

        private void Awake()
        {
            CanvasGroup.DOFade(0f, 1f);
        }

        public override void Show()
        {
            gameObject.SetActive(true);
            CanvasGroup.DOFade(1f, 1f);
            popupTransform.DOScale(Vector3.one, 1f);
        }

        public override void Hide()
        {
            CanvasGroup.DOFade(0f, 1f);
            popupTransform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
            {
                popupTransform.localScale = Vector3.one; 
                gameObject.SetActive(false);
            });
        }
    }
}