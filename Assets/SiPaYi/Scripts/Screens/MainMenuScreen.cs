﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class MainMenuScreen : BaseScreen
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button weaponSelectionButton;
        [SerializeField] private Button characterSelectionButton;
        [SerializeField] private Button settingsButton;

        [SerializeField] private TextMeshProUGUI highScore;
        [SerializeField] private TextMeshProUGUI highScoreEpic;
        [SerializeField] private TextMeshProUGUI credits;

        public override ScreenID ScreenId
        {
            get { return ScreenID.MainMenuScreen; }
        }

        private void OnEnable()
        {
            playButton.onClick.AddListener(OnPlayButton);
            characterSelectionButton.onClick.AddListener(OnCharacterSelection);
            weaponSelectionButton.onClick.AddListener(OnWeaponSelection);
            settingsButton.onClick.AddListener(OnSettings);
            SetViewDetails();
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveListener(OnPlayButton);
            characterSelectionButton.onClick.RemoveListener(OnCharacterSelection);
            weaponSelectionButton.onClick.RemoveListener(OnWeaponSelection);
            settingsButton.onClick.RemoveListener(OnSettings);
        }

        private void OnSettings()
        {
            ScreenHelper.ShowPopup(typeof(SettingsPopup));
        }

        private void OnWeaponSelection()
        {
            ScreenHelper.OpenScreen(typeof(WeaponSelectionScreen));
        }

        private void OnCharacterSelection()
        {
            ScreenHelper.OpenScreen(typeof(CharacterSelectionScreen));
        }

        private void OnPlayButton()
        {
            ScreenHelper.PlayGame();
        }

        private void SetViewDetails()
        {
            credits.text = string.Format("A Game from {0}", Application.companyName);

            SetEpicScore();
            SetHighScore();
        }

        private void SetEpicScore()
        {
            highScoreEpic.text = string.Format("HighScore Epic : {0}", SaveDataHelper.GetEpicScore());
        }

        private void SetHighScore()
        {
            highScore.text = string.Format("HighScore : {0}", SaveDataHelper.GetHighScore());
        }
    }
}