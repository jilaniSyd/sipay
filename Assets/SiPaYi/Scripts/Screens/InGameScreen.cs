﻿namespace KnightHawkStudios.Sipayi
{
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class InGameScreen : BaseScreen, IHeartUpdateListener, ICoinUpdateListener
    {
        [SerializeField] private Button leftButton;
        [SerializeField] private Button rightButton;
        [SerializeField] private Button middleButton;
        [SerializeField] private Button pauseButton;
        [SerializeField] private TextMeshProUGUI coinsCount;

        private GameEntity heartUpdateListener;
        private GameEntity coinUpdateListener;

        public override ScreenID ScreenId
        {
            get
            {
                return ScreenID.InGameHud;
            }
        }

        private void OnEnable()
        {
            leftButton.onClick.AddListener(OnLeftButtonClicked);
            rightButton.onClick.AddListener(OnRightButtonClicked);
            middleButton.onClick.AddListener(OnMiddleButtonClicked);
            pauseButton.onClick.AddListener(OnPauseButtonClicked);

            var gameContext = Contexts.sharedInstance.game;
            heartUpdateListener = gameContext.CreateEntity();
            heartUpdateListener.AddHeartUpdateListener(this);

            coinUpdateListener = gameContext.CreateEntity();
            coinUpdateListener.AddCoinUpdateListener(this);

            SetScore(gameContext.playerEntity.coins.Amount);
        }

        private void SetScore(int amount)
        {
            coinsCount.text = amount.ToString();
        }

        private void OnDisable()
        {
            leftButton.onClick.RemoveListener(OnLeftButtonClicked);
            rightButton.onClick.RemoveListener(OnRightButtonClicked);
            middleButton.onClick.RemoveListener(OnMiddleButtonClicked);
            pauseButton.onClick.RemoveListener(OnPauseButtonClicked);

            if (coinUpdateListener != null)
            {
                coinUpdateListener.isDestroyed = true;
            }
            if (heartUpdateListener != null)
            {
                heartUpdateListener.isDestroyed = true;
            }
        }

        private void OnPauseButtonClicked()
        {
            ScreenHelper.ShowPopup(typeof(PauseMenuPopup));
        }

        private void OnMiddleButtonClicked()
        {
            CreatePlayerJumpEntity();
        }

        private void OnRightButtonClicked()
        {
            CreatePlayerDefendEntity();
        }

        private void OnLeftButtonClicked()
        {
            CreatePlayerAttackEntity();
        }

        private void CreatePlayerAttackEntity()
        {
            var entity = GetInputEntity();
            entity.isPlayerAttack = true;
        }

        private void CreatePlayerJumpEntity()
        {
            var entity = GetInputEntity();
            entity.isPlayerJump = true;
        }

        private void CreatePlayerDefendEntity()
        {
            var entity = GetInputEntity();
            entity.isPlayerDefend = true;
        }

        private InputEntity GetInputEntity()
        {
            var entity = Contexts.sharedInstance.input.CreateEntity();
            entity.isInput = true;
            return entity;
        }

        public void OnCoinUpdate(GameEntity entity, int Amount)
        {
            SetScore(Amount);
        }

        public void OnHeartUpdate(GameEntity entity, int Amount)
        {
        }
    }
}