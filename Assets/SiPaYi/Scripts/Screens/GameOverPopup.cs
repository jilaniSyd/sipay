﻿namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class GameOverPopup : BasePopup
    {
        [Header("Screen Properties")]
        [SerializeField] private Button videoReviveButton;

        [SerializeField] private Button currencyReviveButton;
        [SerializeField] private Button closeButton;
        [SerializeField] private TextMeshProUGUI totalCountText;

        private void OnEnable()
        {
            videoReviveButton.onClick.AddListener(OnVideoRevive);
            currencyReviveButton.onClick.AddListener(OnCurrenctyRevive);
            closeButton.onClick.AddListener(OnClose);
            //TO-DO Update the player value
            totalCountText.text = "Total : " + "3";
            //NOTE : is it right to do here
            Time.timeScale = 0f;
            DOTween.timeScale = 0f;
        }

        private void OnDisable()
        {
            videoReviveButton.onClick.RemoveListener(OnVideoRevive);
            currencyReviveButton.onClick.RemoveListener(OnCurrenctyRevive);
            closeButton.onClick.RemoveListener(OnClose);
            Time.timeScale = 1f;
            DOTween.timeScale = 1f;
        }

        private void OnClose()
        {
            Time.timeScale = 1f;
            DOTween.timeScale = 1f;
            //Show the result screen
            Hide();
            PlayerHelper.SaveCoinsAndEndGame();
        }

        //TODO implement this
        private void OnCurrenctyRevive()
        {
        }

        //TODO implement this
        private void OnVideoRevive()
        {
        }


    }
}