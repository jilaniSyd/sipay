﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;
    using UnityEngine.UI;

    public class PauseMenuPopup : BasePopup
    {
        [SerializeField] private Button closeButton;
        [SerializeField] private Button resumeButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Toggle soundToggle;
        [SerializeField] private Toggle musicToggle;

        private void OnEnable()
        {
            closeButton.onClick.AddListener(OnClose);
            resumeButton.onClick.AddListener(OnResume);
            exitButton.onClick.AddListener(OnExit);
            soundToggle.onValueChanged.AddListener(OnSoundToggle);
            musicToggle.onValueChanged.AddListener(OnMusicToggle);
        }

        private void OnDisable()
        {
            closeButton.onClick.RemoveListener(OnClose);
            resumeButton.onClick.RemoveListener(OnResume);
            exitButton.onClick.RemoveListener(OnExit);
            soundToggle.onValueChanged.RemoveListener(OnSoundToggle);
            musicToggle.onValueChanged.RemoveListener(OnMusicToggle);
        }

        private void OnMusicToggle(bool arg0)
        {
            //TODO:
        }

        private void OnSoundToggle(bool arg0)
        {
            //TODO:
        }

        private void OnExit()
        {
            //End the game
            PlayerHelper.SaveCoinsAndEndGame();
        }

        private void OnResume()
        {
            ScreenHelper.HidePopup(this.GetType());
        }

        private void OnClose()
        {
            ScreenHelper.HidePopup(this.GetType());
        }
    }
}