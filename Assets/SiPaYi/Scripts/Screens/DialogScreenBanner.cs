﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class DialogScreenBanner : MonoBehaviour
    {
        [SerializeField] private Button closeButton;
        [SerializeField] private TextMeshProUGUI coinsText;
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private Screen screenID;

        public Action OnClosed;

        private void OnEnable()
        {
            closeButton.onClick.AddListener(OnClose);
        }

        private void OnDisable()
        {
            closeButton.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            OnClosed?.Invoke();
            if (screenID is BasePopup)
            {
                ScreenHelper.HidePopup(screenID.GetType());
            }
            else
            {
                ScreenHelper.CloseScreen(screenID.GetType());
            }
        }

        public void SetView(BaseScreen screenId, string title, string coins)
        {
            screenID = screenId;
            SetTitle(title);
            SetCoins(coins);
        }

        public void SetTitle(string title)
        {
            titleText.text = title;
        }

        public void SetCoins(string coinsCount)
        {
            coinsText.text = coinsCount;
        }
    }
}