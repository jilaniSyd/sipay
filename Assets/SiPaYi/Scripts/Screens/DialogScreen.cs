﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class DialogScreen : BaseScreen
    {
        [SerializeField]
        private Image characterIconImage;

        [SerializeField]
        private UITextTypeWriter dialogText;

        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private Animator animator;

        public override ScreenID ScreenId => ScreenID.DialogScreen;

        private bool isSequenceShown = false;

        private void OnEnable()
        {
            isSequenceShown = false;
            closeButton.onClick.AddListener(OnClose);
        }

        private void OnDisable()
        {
            closeButton.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            if (!isSequenceShown) { return; }
            if (dialogText.IsTyping)
            {
                dialogText.StopTyping();
            }
            else
            {
                HideConversation();
            }
        }

        public void ShowDialog(Sprite iconSprite, string dialog)
        {
            ShowConversation();
            characterIconImage.sprite = iconSprite;
            dialogText.StartTyping(dialog);
        }

        public void ShowConversation()
        {
            animator.SetTrigger("Show");
            isSequenceShown = true;
        }

        public void HideConversation()
        {
            animator.SetTrigger("Hide");
        }

        public void AnimationFinished()
        {
            Contexts.sharedInstance.game.CreateEntity().isConversationSequenceHide = true;
        }
    }
}