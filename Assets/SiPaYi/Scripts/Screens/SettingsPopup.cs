﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;
    using UnityEngine.UI;

    public class SettingsPopup : BasePopup
    {
        [SerializeField]
        private Toggle musicToggle;

        [SerializeField]
        private Toggle soundToggle;

        [SerializeField]
        private Button removeAds;

        private void OnEnable()
        {
            musicToggle.onValueChanged.AddListener(OnMusicToggle);
            soundToggle.onValueChanged.AddListener(OnSoundToggle);
            removeAds.onClick.AddListener(OnRemoveAds);
        }

        private void OnDisable()
        {
            soundToggle.onValueChanged.RemoveListener(OnSoundToggle);
            musicToggle.onValueChanged.RemoveListener(OnMusicToggle);
            removeAds.onClick.RemoveListener(OnRemoveAds);
        }

        private void OnRemoveAds()
        {
            Debug.Log("Remove Ads");
        }

        private void OnSoundToggle(bool arg0)
        {
            Debug.Log("Toggle Sound");
        }

        private void OnMusicToggle(bool arg0)
        {
            Debug.Log("Toggle Music");
        }
    }
}