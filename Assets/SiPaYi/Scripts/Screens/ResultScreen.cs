﻿namespace KnightHawkStudios.Sipayi
{
    using System;
    using TMPro;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class ResultScreen : BaseScreen
    {
        [SerializeField] private Button middleButton;
        [SerializeField] private Button leftButton;
        [SerializeField] private Button rightButton;

        [SerializeField] private Button leaderBoardButton;

        [SerializeField] private TextMeshProUGUI bestRunText;
        [SerializeField] private TextMeshProUGUI bestEpicRunText;
        [SerializeField] private TextMeshProUGUI totalJumpsText;

        [SerializeField] private DialogScreenBanner dialogBanner;

        public override ScreenID ScreenId
        {
            get { return ScreenID.ResultScreen; }
        }

        private void OnEnable()
        {
            middleButton.onClick.AddListener(OnMiddleButton);
            leftButton.onClick.AddListener(OnLeftButton);
            rightButton.onClick.AddListener(OnRightButton);
            leaderBoardButton.onClick.AddListener(OnLeaderBoard);
            dialogBanner.OnClosed += OnBannerClose;
            SetupView();
        }

        private void OnDisable()
        {
            middleButton.onClick.RemoveListener(OnMiddleButton);
            leftButton.onClick.RemoveListener(OnLeftButton);
            rightButton.onClick.RemoveListener(OnRightButton);
            leaderBoardButton.onClick.RemoveListener(OnLeaderBoard);
            dialogBanner.OnClosed -= OnBannerClose;
        }

        private void OnBannerClose()
        {
            Contexts.sharedInstance.game.CreateEntity().isGameRestart = true;
        }

        private void OnLeaderBoard()
        {
        }

        private void OnRemoveAds()
        {
        }

        private void OnRightButton()
        {
            ScreenHelper.OpenScreen(typeof(CharacterSelectionScreen));
        }

        private void OnLeftButton()
        {
            ScreenHelper.OpenScreen(typeof(WeaponSelectionScreen));
        }

        private void OnMiddleButton()
        {
            SceneManager.LoadSceneAsync("InGame");
        }

        private void SetupView()
        {
            bestEpicRunText.text = string.Format("TOTAL COINS: {0}", SaveDataHelper.GetCoins());
            bestRunText.text = string.Format("BEST RUN : {0} ", SaveDataHelper.GetHighScore());
            totalJumpsText.text = string.Format("TOTAL JUMPS: {0}", 0);
//            bestEpicRunText.text = string.Format("BEST EPIC RUN: {0}", SaveDataHelper.GetEpicScore());
        }
    }
}