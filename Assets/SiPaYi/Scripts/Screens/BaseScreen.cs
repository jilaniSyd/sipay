﻿namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using System;
    using UnityEngine;

    public abstract class BaseScreen : Screen
    {
        public abstract ScreenID ScreenId { get; }

        [Header("Base Settings")]
        [SerializeField]
        private CanvasGroup canvasGroup;

        private Action OnShowComplete;
        private Action OnHideComplete;

        private void Awake()
        {
            gameObject.SetActive(false);
            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0f;
            }
        }

        public override void Show()
        {
            
        }

        public void Show(Action OnComplete = null)
        {
            gameObject.SetActive(true);
            OnShowComplete = OnComplete;
            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0f;
                canvasGroup.DOFade(1f, 1f).OnComplete(() => { OnShowFinished(); });
            }
        }

        public void OnShowFinished()
        {
            OnShowComplete?.Invoke();
        }

        public void OnHideFinished()
        {
            OnHideComplete?.Invoke();
            OnCompleteFade(false);
        }

        public void Hide(Action OnComplete = null)
        {
            OnHideComplete = OnComplete;
            if (canvasGroup != null)
            {
                canvasGroup.DOFade(0f, 0.5f).OnComplete(OnHideFinished);
            }
            else
            {
                OnCompleteFade(false);
            }
        }

        private void OnCompleteFade(bool canShow)
        {
            if (gameObject != null)
            {
                gameObject.SetActive(canShow);
            }
        }
    }
}