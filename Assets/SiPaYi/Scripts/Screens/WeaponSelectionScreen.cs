﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using System.Linq;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class WeaponSelectionScreen : BaseScreen, IWeaponSelectedListener
    {
        [SerializeField] private TextMeshProUGUI weaponNameText;
        [SerializeField] private TextMeshProUGUI weaponDetailsText;
        [SerializeField] private WeaponsListDisplayItem weaponListDisplayItemPrefab;
        [SerializeField] private Transform displayItemsParent;
        [SerializeField] private DialogScreenBanner banner;

        private GameEntity weaponSelectListenerEntity;
        private int selectedId = -1;
        private Dictionary<int, WeaponsData> data;

        private List<WeaponDisplayItem> weaponsListItems = new List<WeaponDisplayItem>();

        public override ScreenID ScreenId { get { return ScreenID.WeaponSelection; } }

        private void OnEnable()
        {
            weaponSelectListenerEntity = Contexts.sharedInstance.game.CreateEntity();
            weaponSelectListenerEntity.AddWeaponSelectedListener(this);

            SetSelectedItem(SaveDataHelper.GetWeaponId());
            banner.SetTitle("WEAPONS");
            banner.SetTitle("");
        }

        private void OnDisable()
        {
            if (weaponSelectListenerEntity != null)
            {
                weaponSelectListenerEntity.Destroy();
            }
        }

        private void Start()
        {
            data = Contexts.sharedInstance.game.weaponsBalancingData.Data;
            SetupView(data.Values.ToList());
            SetSelectedItem(SaveDataHelper.GetWeaponId());
        }

        //TODO Could be done in a better way. This is the dirty way
        private void SetupView(List<WeaponsData> weaponsList)
        {
            var weaponsListCount = weaponsList.Count / 3;
            int index = 0;
            for (int i = 0; i < weaponsListCount; i++)
            {
                var itemsList = weaponsList.GetRange(index, 3);
                var item = CreateDisplayList(itemsList);
                weaponsListItems.AddRange(item);
                index += 3;
            }
            var remainingItems = weaponsList.Count % 3;
            if (remainingItems > 0)
            {
                var lastWeaponsList = weaponsList.GetRange(index, remainingItems);
                var lastItems = CreateDisplayList(lastWeaponsList);
                weaponsListItems.AddRange(lastItems);
            }
        }

        private List<WeaponDisplayItem> CreateDisplayList(List<WeaponsData> weaponsList)
        {
            var item = Instantiate(weaponListDisplayItemPrefab, displayItemsParent, false);
            return item.Setup(weaponsList);
        }

        public void OnWeaponSelected(GameEntity entity, int Id)
        {
            if (!selectedId.Equals(Id))
            {
                var previousSelectedItem = weaponsListItems.Find(x => x.Id.Equals(selectedId));
                if (previousSelectedItem != null)
                {
                    previousSelectedItem.UnSelect();
                }
                selectedId = Id;
                SetSelectedItem(selectedId);
            }
        }

        private void SetSelectedItem(int selectedId)
        {
            if (selectedId == -1)
            {
                weaponNameText.text = "";
                weaponDetailsText.text = "";
                return;
            }

            if (data == null) { return; }
            var weaponItem = data[selectedId];
            if (weaponItem != null)
            {
                weaponNameText.text = weaponItem.Name.ToUpper();
                weaponDetailsText.text = string.Format("Weapon Does {0} damage", weaponItem.Damage);
            }
            else
            {
                weaponNameText.text = "";
                weaponDetailsText.text = "";
            }
            UnSelectAllAndSetSelected(selectedId);
        }

        private void UnSelectAllAndSetSelected(int selectedId)
        {
            foreach (WeaponDisplayItem item in weaponsListItems)
            {
                if (item.Id == selectedId)
                {
                    item.Select();
                }
                else
                {
                    item.UnSelect();
                }
            }
        }
    }
}