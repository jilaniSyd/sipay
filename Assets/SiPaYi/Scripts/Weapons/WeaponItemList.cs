﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using UnityEngine;

    public class WeaponItemList : ScriptableObject
    {
        public List<WeaponItem> weaponItems;
    }
}