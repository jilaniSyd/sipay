﻿namespace KnightHawkStudios.Sipayi
{
    using System;

    [Serializable]
    public class WeaponItem
    {
        public string name;
        public float damage;
        public string damageType;
    }
}