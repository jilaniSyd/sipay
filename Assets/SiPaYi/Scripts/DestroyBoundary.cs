﻿namespace KnightHawkStudios.Sipayi
{
    using DG.Tweening;
    using UnityEngine;

    public class DestroyBoundary : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER)
            {
                if (Contexts.sharedInstance.game.isPlayer)
                {
                    var playerEntity = Contexts.sharedInstance.game.playerEntity;
                    if (!playerEntity.isDead)
                    {
                        playerEntity.isAboutToDie = true;
                    }
                }
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_PLAYER) { return; }
            ReturnToPool(other.gameObject);
        }

        private void ReturnToPool(GameObject collidedGameObject)
        {
            //TODO Move to pool
            DOVirtual.DelayedCall(1f, () => { Destroy(collidedGameObject); });
        }
    }
}