﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections;
    using TMPro;
    using UnityEngine;

    public class UITextTypeWriter : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI txt;

        private string story;

        private bool isTyping = false;

        public bool IsTyping { get => isTyping; set => isTyping = value; }

        private void Awake()
        {
            story = txt.text;
            txt.text = "";
        }

        private IEnumerator PlayText()
        {
            IsTyping = true;
            foreach (char c in story)
            {
                txt.text += c;
                yield return new WaitForSeconds(0.05f);
            }
            IsTyping = false;
        }

        public void StartTyping(string story)
        {
            StopTyping();
            this.story = story;
            txt.text = "";
            StartCoroutine(PlayText());
        }

        public void StopTyping()
        {
            IsTyping = false;
            StopAllCoroutines();
            txt.text = story;
        }
    }
}