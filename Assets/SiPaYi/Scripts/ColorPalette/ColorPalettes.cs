using System.Collections.Generic;
using UnityEngine;

namespace KnightHawkStudios.Sipayi
{
    public class ColorPalettes : ScriptableObject
    {
        public List<Color> colors;
    }
}