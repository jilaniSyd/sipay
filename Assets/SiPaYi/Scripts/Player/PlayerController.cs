﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;

    public class PlayerController : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constants.TAG_GROUND)
            {
                Contexts.sharedInstance.game.CreateEntity().AddGrounded(true);
            }

            if (other.tag == Constants.TAG_OBSTACLE_TRAP)
            {
                Contexts.sharedInstance.game.CreateEntity().isHealthPortionLost = true;
            }

            if (other.tag == Constants.TAG_BIRD_EGG)
            {
                Contexts.sharedInstance.game.CreateEntity().isHealthPortionLost = true;
            }
        }
    }
}