﻿using System.Collections.Generic;
using KnightHawkStudios.Sipayi;
using UnityEngine;

public static class PlayerHelper
{
    public static List<Vector3> GetJumpWayPoints(Vector3 start, Vector3 end)
    {
        //https://javascript.info/bezier-curve
        //Formula for a 3 point curve
        //P = (1−t)2P1 + 2(1−t)tP2 + t2P3

        var points = new List<Vector3>();
        var point1 = start;
        var point2 = new Vector3((end.x - start.x) / 2, start.y + ((end.y - start.y) / 2) + 5f, 0);
        var point3 = end;
        for (int i = 0; i < 10; i++)
        {
            var factor = i * (float)(1.0 / 10.0);
            var xPoint = ((1 - factor) * (1 - factor) * point1.x) + (2 * (1 - factor) * factor * point2.x) + (factor * factor * point3.x);
            var yPoint = ((1 - factor) * (1 - factor) * point1.y) + (2 * (1 - factor) * factor * point2.y) + (factor * factor * point3.y);
            var point = new Vector3(xPoint, yPoint, 0);
            points.Add(point);
            Debug.Log($"Point : {point} at factor {factor}");
        }
        return points;
    }

    public static void SaveCoinsAndEndGame()
    {
        SaveCoinsAndJumps();
        ScreenHelper.OpenScreen(typeof(ResultScreen));
        Contexts.sharedInstance.game.playerEntity.isDead = true;
    }
    
    private static void SaveCoinsAndJumps()
    {
        var playerEntity = Contexts.sharedInstance.game.playerEntity;
        if (playerEntity.hasCoins)
        {
            var totalCoinsCollected = playerEntity.coins.Amount;
            var coinsSaveEntity = Contexts.sharedInstance.game.CreateEntity();
            coinsSaveEntity.AddSavePlayerCoins(totalCoinsCollected);
        }
    }
}