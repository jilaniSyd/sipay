﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KnightHawkStudios.Sipayi
{
    public static class ScreenHelper
    {
        public static BaseScreen GetScreen(Type t)
        {
            return FindScreen<BaseScreen>(t);
        }

        public static BasePopup GetPopup(Type t)
        {
            return FindPopup<BasePopup>(t);
        }

        private static T LoadPopup<T>(Type t)
        {
            var path = "Prefabs/Popups/" + t.Name;
            var basePopup = Resources.Load(path) as GameObject;
            if (basePopup == null) { return default(T); }

            var popup = UnityEngine.Object.Instantiate(basePopup);
            popup.name = t.Name;
            return popup.GetComponent<T>();
        }

        private static T LoadScreen<T>(Type t)
        {
            var path = "Prefabs/Screens/" + t.Name;
            var baseScreen = Resources.Load(path) as GameObject;
            if (baseScreen != null)
            {
                var instance = UnityEngine.Object.Instantiate(baseScreen);
                instance.name = t.Name;
                return instance.GetComponent<T>();
            }
            return default(T);
        }

        private static BaseScreen FindScreen<T>(Type t)
        {
            var canvas = GameObject.Find("Canvas");
            Debug.Assert(canvas != null);
            var screen = canvas.transform.Find(t.Name);
            if (screen == null)
            {
                var newScreen = LoadScreen<T>(t) as BaseScreen;
                newScreen.transform.SetParent(canvas.transform, false);

                return newScreen;
            }
            return screen.GetComponent<T>() as BaseScreen;
        }

        private static BasePopup FindPopup<T>(Type t)
        {
            var canvas = GameObject.Find("Canvas");
            Debug.Assert(canvas != null);
            //TODO: Find by component. not by name of the game object.
            //Very possible name could contains clone or other strings
            var screen = canvas.transform.Find(t.Name);
            if (screen == null)
            {
                var newScreen = LoadPopup<T>(t) as BasePopup;
                newScreen.transform.SetParent(canvas.transform, false);

                return newScreen;
            }
            return screen.GetComponent<T>() as BasePopup;
        }

        public static void OpenScreen(Type t, Action OnComplete = null)
        {
            var openDialogEntity = Contexts.sharedInstance.game.CreateEntity();
            openDialogEntity.isScreen = true;
            openDialogEntity.AddScreenShow(t);
            openDialogEntity.AddActionEvent(OnComplete);
        }

        public static void CloseScreen(Type t)
        {
            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.isScreen = true;
            entity.AddScreenClose(t);
        }

        public static void ShowPopup(Type t)
        {
            var popEntity = Contexts.sharedInstance.game.CreateEntity();
            popEntity.isPopup = true;
            popEntity.AddPopupShow(t);
        }

        public static void HidePopup(Type t)
        {
            var popEntity = Contexts.sharedInstance.game.CreateEntity();
            popEntity.isPopup = true;
            popEntity.AddPopupHide(t);
        }

        public static void PlayGame()
        {
            SceneManager.LoadSceneAsync("InGame");
        }

        public static void GotoMainMenu()
        {
            SceneManager.LoadSceneAsync("MainMenu");
        }
    }
}