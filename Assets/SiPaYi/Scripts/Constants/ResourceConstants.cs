﻿namespace KnightHawkStudios.Sipayi
{
    public static class ResourceConstants
    {
        public static readonly string WEAPONS_SO_PATH = "BalancingData/WeaponsSO";
        public static readonly string CHARACTERS_SO_PATH = "BalancingData/CharactersSO";
        public static readonly string LEVEL_CONFIG_PATH = "Config/LevelConfig";
        public static readonly string DIALOGICONS_CONFIG_PATH = "Config/DialogIconsConfig";

        public static readonly string BLOCKS_PATH = "LevelData/";
        public static readonly string LEVEL_GO_NAME = "LevelBlocks";
        public static readonly string LEVEL_SORTED_DATA_PATH = "SO/LevelSortedData";
        public static readonly string CONVERSATIONS_DATA_PATH = "BalancingData/DialogsSO";
    }
}