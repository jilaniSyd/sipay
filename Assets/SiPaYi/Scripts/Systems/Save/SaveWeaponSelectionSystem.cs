﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using UnityEngine;

public class SaveWeaponSelectionSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public SaveWeaponSelectionSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.WeaponSelected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            PlayerPrefs.SetInt(Constants.WEAPON_ID, entity.weaponSelected.Id);
            gameContext.ReplaceWeaponSelectedId(entity.weaponSelected.Id);
        }
    }
}