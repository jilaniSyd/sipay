﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

public class SaveCoinsSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public SaveCoinsSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.SavePlayerCoins);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            UnityEngine.PlayerPrefs.SetInt(Constants.PLAYER_COINS, entity.savePlayerCoins.Amount);

            gameContext.playerCoins.Amount = entity.savePlayerCoins.Amount;

            entity.isDestroyed = true;
        }
    }
}