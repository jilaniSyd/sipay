﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using UnityEngine;

public class SaveCharacterSelectionSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public SaveCharacterSelectionSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.CharacterSelected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            PlayerPrefs.SetInt(Constants.CHARACTER_ID, entity.characterSelected.Id);
            gameContext.ReplaceCharacterSelectedId(entity.characterSelected.Id);
        }
    }
}