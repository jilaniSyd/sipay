﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

public class SaveHighScoreSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public SaveHighScoreSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.SavePlayerScore);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            UnityEngine.PlayerPrefs.SetInt(Constants.PLAYER_HIGHSCORE, entity.savePlayerScore.Amount);

            gameContext.playerHighScore.Amount = entity.savePlayerScore.Amount;

            entity.isDestroyed = true;
        }
    }
}