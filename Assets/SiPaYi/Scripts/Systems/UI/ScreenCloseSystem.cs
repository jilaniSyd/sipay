﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

public class ScreenCloseSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public ScreenCloseSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.ScreenClose);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isScreen;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.hasScreenStack)
            {
                var screenStack = gameContext.screenStackEntity.screenStack.Stack;
                var screen = ScreenHelper.GetScreen(entity.screenClose.SceneName);
                if (screenStack.Contains(screen))
                {
                    var topScreen = screenStack.Pop();
                    topScreen.Hide();
                    if (screenStack.Count > 0)
                    {
                        var screenToShow = screenStack.Peek();
                        screenToShow.Show();
                    }
                }
            }
            entity.isDestroyed = true;
        }
    }
}