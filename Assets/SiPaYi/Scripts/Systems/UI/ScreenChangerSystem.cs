﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ScreenChangerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public ScreenChangerSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.ScreenShow.Added());
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isScreen;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if (gameContext.hasScreenStack)
            {
                var stackEntity = gameContext.screenStackEntity;
                PushToStack(stackEntity, e.screenShow.Screen, e.actionEvent.action);
            }
            else
            {
                var stactEntity = gameContext.CreateEntity();
                stactEntity.ReplaceScreenStack(new Stack<BaseScreen>());
                PushToStack(stactEntity, e.screenShow.Screen, e.actionEvent.action);
            }
        }
    }

    private void PushToStack(GameEntity entity, Type t, Action OnComplete)
    {
        if (entity.screenStack.Stack.Count > 0)
        {
            var screenToHide = entity.screenStack.Stack.Peek();
            if (!(screenToHide is MainMenuScreen))
            {
                entity.screenStack.Stack.Pop();
            }
            screenToHide.Hide();
        }
        var screen = ScreenHelper.GetScreen(t);
        screen.Show(OnComplete);
        entity.screenStack.Stack.Push(screen);
    }
}