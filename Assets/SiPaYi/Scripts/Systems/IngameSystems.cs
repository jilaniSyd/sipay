﻿public class IngameSystems : Feature
{
    public IngameSystems(Contexts contexts)
    {
        Add(new GameInitializeSystem(contexts));
        Add(new CharacterBalancingDataSystem(contexts));
        Add(new WeaponBalancingDataSystem(contexts));
        Add(new DialogInitSystem(contexts));
        Add(new SaveDataInitSystem(contexts));
        Add(new SaveSystems(contexts));
        Add(new DialogShowSystem(contexts));
        Add(new ScreenChangerSystem(contexts));
        Add(new ScreenCloseSystem(contexts));
        Add(new PopupShowSystem(contexts));
        Add(new LevelSystems(contexts));
        Add(new PlayerSystems(contexts));
        Add(new InputSystems(contexts));
        Add(new CameraMovementSystem(contexts));
        Add(new CharacterSelectSystem(contexts));
        Add(new WeaponSelectSystem(contexts));
        Add(new GameEventSystems(contexts));
        Add(new GameDestroySystem(contexts));
        Add(new GameRestartSystem(contexts));
    }
}