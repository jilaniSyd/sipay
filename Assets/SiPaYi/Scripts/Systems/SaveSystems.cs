﻿public class SaveSystems : Feature
{
    public SaveSystems(Contexts contexts)
    {
        Add(new SaveCoinsSystem(contexts));
        Add(new SaveHighScoreSystem(contexts));
        Add(new SaveWeaponSelectionSystem(contexts));
        Add(new SaveCharacterSelectionSystem(contexts));
    }
}