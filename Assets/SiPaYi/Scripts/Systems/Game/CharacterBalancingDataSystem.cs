﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBalancingDataSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public CharacterBalancingDataSystem(Contexts contexts)
    {
        gameContext = contexts.game;
    }

    public void Initialize()
    {
        if (gameContext.hasCharacterBalancingData) { return; }

        var data = GameObject.Instantiate(Resources.Load(ResourceConstants.CHARACTERS_SO_PATH)) as Characters;
        var dictionary = new Dictionary<int, CharactersData>();
        foreach (var charData in data.dataArray)
        {
            var id = Convert.ToInt32(charData.Id);
            dictionary.Add(id, charData);
        }

        gameContext.ReplaceCharacterBalancingData(dictionary);
    }
}