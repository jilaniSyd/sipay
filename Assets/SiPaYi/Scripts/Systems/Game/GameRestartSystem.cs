﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using KnightHawkStudios.Sipayi;

public class GameRestartSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public GameRestartSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.GameRestart);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isGameRestart;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var allEntities = gameContext.GetEntities();
            foreach (var gameEntity in allEntities)
            {
                gameEntity.isDestroyed = true;
            }

            ScreenHelper.GotoMainMenu();
        }
    }
}