﻿using DG.Tweening;
using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public CameraMovementSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.CameraMove);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var cameraTransform = Camera.main.transform;
            var playerTargetPosition = gameContext.playerEntity.playerTargetPosition.position;
            cameraTransform.DOMoveX(playerTargetPosition.x, 0.25f, false);
            entity.isDestroyed = true;
        }
    }
}