﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogInitSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public DialogInitSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        if (gameContext.hasConversationDialogBalancingData) { return; }

        var data = GameObject.Instantiate(Resources.Load(ResourceConstants.CONVERSATIONS_DATA_PATH)) as Dialogs;
        if (data == null)
        {
            Debug.Log("No DialogsSO found"); return;
        }

        var dictionary = new Dictionary<string, ConversationData>();

        var index = -1;
        foreach (var dialogs in data.dataArray)
        {
            if (dialogs.Dialogid == "--")
            {
                var conversationData = dictionary.ElementAt(index).Value.dialogs;
                conversationData.Add(new ConversationSequenceData(dialogs));
                continue;
            }

            if (!dictionary.ContainsKey(dialogs.Dialogid))
            {
                var conversationData = new ConversationData();
                conversationData.dialogs.Add(new ConversationSequenceData(dialogs));
                dictionary.Add(dialogs.Dialogid, conversationData);
                index++;
            }
        }
        Contexts.sharedInstance.game.SetConversationDialogBalancingData(dictionary);
    }
}