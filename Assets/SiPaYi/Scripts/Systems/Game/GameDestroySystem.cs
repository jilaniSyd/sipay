﻿using Entitas;
using System.Collections.Generic;

public class GameDestroySystem : ReactiveSystem<GameEntity>
{
    public GameDestroySystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Destroyed);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.Destroy();
        }
    }
}