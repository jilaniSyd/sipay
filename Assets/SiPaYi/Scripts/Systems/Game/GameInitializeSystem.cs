﻿using DG.Tweening;
using Entitas;
using KnightHawkStudios.Sipayi;
using UnityEngine.SceneManagement;

public class GameInitializeSystem : IInitializeSystem
{
    public GameInitializeSystem(Contexts context)
    {
    }

    public void Initialize()
    {
        //TODO : Not the best way to do it
        ClearStack();
        DOVirtual.DelayedCall(1f, ShowMainMenu);
    }

    private void ClearStack()
    {
        if (Contexts.sharedInstance.game.hasScreenStack)
        {
            Contexts.sharedInstance.game.screenStackEntity.Destroy();
        }
    }

    private void ShowMainMenu()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            ScreenHelper.OpenScreen(typeof(MainMenuScreen));
        }
        else
        {
            ScreenHelper.OpenScreen(typeof(InGameScreen));
        }
    }

    private GameEntity GetScreenEntity()
    {
        var entity = Contexts.sharedInstance.game.CreateEntity();
        entity.isScreen = true;
        return entity;
    }
}