﻿using Entitas;
using UnityEngine;
using System.Collections.Generic;

public class UnLoadBlocksSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public UnLoadBlocksSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.UnloadBlock);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var levelBlock = entity.unloadBlock.levelBlock;
            Debug.Log(levelBlock.name);

            //TODO: possibly the order of deleting the object could be a problem.
            //Need to find a better way to do it
            var levelBlocks = gameContext.levelDataEntity.levelBlocks.Blocks;
            levelBlocks.Remove(levelBlock);
            gameContext.levelDataEntity.ReplaceLevelBlocks(levelBlocks);
            //TODO: return to pool
            GameObject.Destroy(levelBlock.gameObject);
            entity.isDestroyed = true;
        }
    }
}