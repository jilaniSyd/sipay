﻿using Entitas;

public class LevelDataResetSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public LevelDataResetSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        if (gameContext.isLevelData)
        {
            var levelData = gameContext.levelDataEntity;
            levelData.Destroy();
        }
    }
}