﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using KnightHawkStudios.Sipayi;
using UnityEngine.SceneManagement;


public class LevelFindTransformSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public LevelFindTransformSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        //TODO: Not the best way to do this? could move these initialize systems only to ingame systems
        if (SceneManager.GetActiveScene().name == "InGame")
        {
            var levelTransform = GameObject.Find(ResourceConstants.LEVEL_GO_NAME);
            if (levelTransform == null)
            {
                Debug.LogError("Level GO not found");
                return;
            }

            var levelDataEntity = gameContext.levelDataEntity;
            levelDataEntity.ReplaceTransform(levelTransform.transform);
        }
    }
}