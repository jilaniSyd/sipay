﻿using Entitas;
using KnightHawkStudios.Sipayi;
using UnityEngine;

public class LevelConfigInitSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public LevelConfigInitSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        if (gameContext.isLevelData && !gameContext.levelDataEntity.hasLevelConfig)
        {
            SetLevelConfigData();
        }
        else
        {
            gameContext.isLevelData = true;
            SetLevelConfigData();
        }
        SetLevelSortedData();
        gameContext.CreateEntity().isLoadBlocks = true;
    }

    private LevelConfig GetLevelConfig()
    {
        var levelConfig = Resources.Load<LevelConfig>(ResourceConstants.LEVEL_CONFIG_PATH) as LevelConfig;
        var levelConfigObject = GameObject.Instantiate(levelConfig) as LevelConfig;
        if (levelConfigObject == null)
        {
            Debug.LogError("LevelConfig  not loaded");
            return null;
        }
        return levelConfigObject;
    }

    private void SetLevelConfigData()
    {
        var levelDataEntity = gameContext.levelDataEntity;
        var levelConfig = GetLevelConfig();
        if (levelConfig == null)
        {
            Debug.LogError("Couldnt load Config");
            return;
        }
        levelDataEntity.ReplaceLevelConfig(levelConfig);
    }

    private void SetLevelSortedData()
    {
        var levelDataEntity = gameContext.levelDataEntity;
        var levelSortedDataResource = Resources.Load<LevelSortedData>(ResourceConstants.LEVEL_SORTED_DATA_PATH) as LevelSortedData;
        var levelSortedDataObject = GameObject.Instantiate(levelSortedDataResource) as LevelSortedData;
        if (levelSortedDataObject == null)
        {
            Debug.LogError("Level Sorted DataObject couldnt be found");
            return;
        }
        levelDataEntity.ReplaceLevelSortedData(levelSortedDataObject);
    }
}