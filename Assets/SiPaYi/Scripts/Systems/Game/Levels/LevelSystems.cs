﻿public class LevelSystems : Feature
{
    public LevelSystems(Contexts contexts)
    {
        Add(new LevelDataResetSystem(contexts));
        Add(new LevelConfigInitSystem(contexts));
        Add(new LevelFindTransformSystem(contexts));
        Add(new LoadBlocksSystem(contexts));
        Add(new LoadBlocksCheckSystem(contexts));
        Add(new UnLoadBlocksSystem(contexts));
    }
}