﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using UnityEngine;

public class LoadBlocksSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public LoadBlocksSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.LoadBlocks);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var levelBlocks = LoadLevelBlocks();
            SetLevelBlocks(levelBlocks);
            entity.isDestroyed = true;
        }
    }

    private void SetLevelBlocks(List<LevelBlock> levelBlocksGenerated)
    {
        if (gameContext.levelDataEntity.hasLevelBlocks)
        {
            var levelBlocksFromGame = gameContext.levelDataEntity.levelBlocks;
            levelBlocksFromGame.Blocks.AddRange(levelBlocksGenerated);
        }
        else
        {
            gameContext.levelDataEntity.AddLevelBlocks(levelBlocksGenerated);
        }
    }

    //TODO: Refactor
    private List<LevelBlock> LoadLevelBlocks()
    {
        var levelConfigIndex = 0;
        var levelDataEntity = gameContext.levelDataEntity;
        var levelConfig = levelDataEntity.levelConfig.Config;
        var blockSize = levelConfig.LoadSize;
        var levelSortedData = levelDataEntity.levelSortedData.SortedData;
        var levelBlockTransform = levelDataEntity.transform.transform;

        List<LevelBlock> levelBlocks = new List<LevelBlock>();
        if (levelDataEntity.hasLevelBlockIndex)
        {
            levelConfigIndex = levelDataEntity.levelBlockIndex.Index;
        }

        for (int i = 0; i < blockSize; i++)
        {
            //TODO LOAD asynchronously and use pooling
            if (levelConfigIndex >= levelConfig.LevelDifficulties.Count)
            {
                levelConfigIndex = 0;
            }
            var difficulty = levelConfig.LevelDifficulties[levelConfigIndex];
            var levelBlock = GetLevelBlock(levelSortedData, difficulty);
            var instantiatedBlock = GameObject.Instantiate(levelBlock, levelBlockTransform, false);
            if (levelDataEntity.hasLevelSpawnPoint)
            {
                var offset = levelDataEntity.levelSpawnPoint.xOffset;
                instantiatedBlock.transform.position = new Vector3(offset, 0f, 0f);
                offset += instantiatedBlock.Length;
                levelDataEntity.ReplaceLevelSpawnPoint(offset);
            }
            else
            {
                var offset = 0f;
                instantiatedBlock.transform.position = new Vector3(offset, 0f, 0f);
                offset += instantiatedBlock.Length;
                levelDataEntity.ReplaceLevelSpawnPoint(offset);
            }

            levelConfigIndex++;
            levelDataEntity.ReplaceLevelBlockIndex(levelConfigIndex);

            levelBlocks.Add(instantiatedBlock);
        }

        return levelBlocks;
    }

    private LevelBlock GetLevelBlock(LevelSortedData levelSortedData, LevelDifficulty difficulty)
    {
        var difficultyList = levelSortedData.Data.Find(x => x._levelDifficulty == difficulty);
        return difficultyList._levelCollection[UnityEngine.Random.Range(0, difficultyList._levelCollection.Count)];
    }
}