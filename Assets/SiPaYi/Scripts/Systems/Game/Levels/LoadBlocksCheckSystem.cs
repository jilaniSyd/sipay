﻿using Entitas;
using System.Collections.Generic;

public class LoadBlocksCheckSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public LoadBlocksCheckSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.BlockEndReached);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var levelConfig = gameContext.levelDataEntity.levelConfig.Config;
            var levelBlocks = gameContext.levelDataEntity.levelBlocks.Blocks;

            if (levelBlocks.Count <= levelConfig.LoadOffset)
            {
                gameContext.CreateEntity().isLoadBlocks = true;
            }
            entity.isDestroyed = true;
        }
    }
}