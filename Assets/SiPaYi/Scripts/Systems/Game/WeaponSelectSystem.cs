﻿using Entitas;
using System.Collections.Generic;

public class WeaponSelectSystem : ReactiveSystem<GameEntity>
{
    public WeaponSelectSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.WeaponSelected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            //TODO : Save the selected weapon to the playerpref
        }
    }
}