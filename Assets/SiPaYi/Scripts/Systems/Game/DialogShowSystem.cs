﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using UnityEngine;

public class DialogShowSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public DialogShowSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.ConversationDialogShow.Added(), GameMatcher.ConversationSequenceHide.Added());
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.hasConversationDialogShow)
            {
                ProcessDialogShowRequest(entity);
            }

            if (entity.isConversationSequenceHide)
            {
                ProcessSequenceHideRequest();
            }
        }
    }

    private void ProcessSequenceHideRequest()
    {
        var seqDataEntity = gameContext.conversationDialogEntity;
        var seqData = seqDataEntity.conversationSequenceData.Data;
        var index = seqDataEntity.conversationSequenceIndex.Index;
        index++;
        if (index >= seqData.Count)
        {
            ScreenHelper.OpenScreen(typeof(InGameScreen));
            seqDataEntity.Destroy();
        }
        else
        {
            var seq = seqData[index];
            var dialogScreen = (DialogScreen)ScreenHelper.GetScreen(typeof(DialogScreen));
            dialogScreen.ShowDialog(GetSpriteForConversation(seq.characterType), seq.text);
            seqDataEntity.ReplaceConversationSequenceIndex(index);
        }
    }

    private void ProcessDialogShowRequest(GameEntity entity)
    {
        var sequenceId = entity.conversationDialogShow.SequenceId;

        var conversationSO = gameContext.conversationDialogBalancingData.conversationData;
        if (!conversationSO.ContainsKey(sequenceId))
        {
            Debug.LogError("SequenceID is not found in the Dialogs SO data");
        }
        var sequences = conversationSO[sequenceId].dialogs;
        var sequenceEntity = gameContext.CreateEntity();
        var index = 0;
        var sequenceData = sequences[index];
        sequenceEntity.isConversationDialog = true;
        sequenceEntity.AddConversationSequenceIndex(index);
        sequenceEntity.AddConversationSequenceData(sequences);
        var conversationIcon = GetSpriteForConversation(sequenceData.characterType);

        ScreenHelper.OpenScreen(typeof(DialogScreen), () =>
        {
            var dialogScreen = (DialogScreen)ScreenHelper.GetScreen(typeof(DialogScreen));
            dialogScreen.ShowDialog(conversationIcon, sequenceData.text);
        });
    }

    private Sprite GetSpriteForConversation(CharacterType characterType)
    {
        var iconsConfig = GetIconsConfig();
        var iconsData = iconsConfig.Icons.Find(x => x.characterType == characterType);
        return iconsData.Sprite;
    }

    //TODO save the reference
    private DialogIconsConfig GetIconsConfig()
    {
        var iconsConfig = Resources.Load<DialogIconsConfig>(ResourceConstants.DIALOGICONS_CONFIG_PATH) as DialogIconsConfig;
        var iconsConfigObject = GameObject.Instantiate(iconsConfig) as DialogIconsConfig;
        if (iconsConfigObject == null)
        {
            Debug.LogError("LevelConfig  not loaded");
            return null;
        }
        return iconsConfigObject;
    }
}