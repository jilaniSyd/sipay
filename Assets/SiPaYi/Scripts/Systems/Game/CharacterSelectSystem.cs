﻿using Entitas;
using System.Collections.Generic;

public class CharacterSelectSystem : ReactiveSystem<GameEntity>
{
    public CharacterSelectSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.CharacterSelected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            //Save selected character to PlayerPref
        }
    }
}