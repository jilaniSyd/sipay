﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

public class PopupShowSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PopupShowSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.PopupShow.Added(), GameMatcher.PopupHide.Added());
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isPopup;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.hasPopupShow)
            {
                ShowPopup(entity.popupShow.Popup);
            }

            if (entity.hasPopupHide)
            {
                HidePopup(entity.popupHide.Popup);
            }
        }
    }

    private void ShowPopup(System.Type t)
    {
        if (gameContext.hasScreenStack)
        {
            var stackEntity = gameContext.screenStackEntity;
            var lastOpenedScreen = stackEntity.screenStack.Stack.Peek();
            lastOpenedScreen.Hide();
        }
        var popup = ScreenHelper.GetPopup(t);
        popup.Show();
    }

    private void HidePopup(System.Type t)
    {
        var popup = ScreenHelper.GetPopup(t);
        popup.Hide();

        if (gameContext.hasScreenStack)
        {
            var stackEntity = gameContext.screenStackEntity;
            var lastOpenedScreen = stackEntity.screenStack.Stack.Peek();
            lastOpenedScreen.Show();
        }
    }
}