﻿using Entitas;
using System.Collections.Generic;

public class PlayerFindNextTargetSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerFindNextTargetSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Grounded);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.grounded.Grounded)
            {
                var playerEntity = gameContext.playerEntity;
                var currentIndex = playerEntity.playerInBlockIndex.Index;
                var currentBlock = playerEntity.playerInBlock.levelBlock;
                var nextBlock = playerEntity.playerNextBlock.levelBlock;
                var offset = new UnityEngine.Vector3(0.0f, 0.5f, 0f);
                if (currentIndex < currentBlock.Length)
                {
                    var nextTarget = currentBlock.obstacles[currentIndex];
                    playerEntity.ReplacePlayerTargetPosition(nextTarget.transform.position + offset);
                    playerEntity.ReplacePlayerInBlockIndex(currentIndex + 1);
                }
                else
                {
                    playerEntity.ReplacePlayerTargetPosition(nextBlock.obstacles[0].transform.position + offset);
                    playerEntity.ReplacePlayerInBlockIndex(0);
                }
                UnityEngine.Debug.Log(string.Format("Find Next Target {0}", playerEntity.playerTargetPosition.position));
            }
        }
    }
}