﻿using Entitas;
using System.Collections.Generic;

public class PlayerHealthPortionCollectSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerHealthPortionCollectSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.HealthPortionCollected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.isPlayer)
            {
                var playerEntity = gameContext.playerEntity;
                if (playerEntity.lives.Amount < 3)
                {
                    playerEntity.lives.Amount++;

                    var healthUpdateEntity = gameContext.CreateEntity();
                    healthUpdateEntity.AddHeartUpdate(playerEntity.lives.Amount);
                }
            }
            entity.isDestroyed = true;
        }
    }
}