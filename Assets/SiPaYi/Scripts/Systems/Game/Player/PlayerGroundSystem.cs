﻿using Entitas;
using System.Collections.Generic;

public class PlayerGroundSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerGroundSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Grounded);
    }

    protected override bool Filter(GameEntity entity)
    {
        return !entity.isPlayer;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.playerEntity.hasGrounded)
            {
                gameContext.playerEntity.grounded.Grounded = entity.grounded.Grounded;
            }
            entity.isDestroyed = true;
        }
    }
}