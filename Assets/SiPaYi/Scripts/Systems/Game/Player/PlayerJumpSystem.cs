﻿using DG.Tweening;
using Entitas;
using System.Collections.Generic;

public class PlayerJumpSystem : ReactiveSystem<GameEntity>
{
    public GameContext gameContext;

    public PlayerJumpSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.PlayerJump);
    }

    protected override bool Filter(GameEntity entity)
    {
        return gameContext.isPlayer && !gameContext.playerEntity.isDead && gameContext.playerEntity.grounded.Grounded;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var playerEntity = gameContext.playerEntity;
            var playerTransform = playerEntity.transform.transform;
            var playerRigidbody = playerEntity.rigidbody2D.rigidbody2D;
            var targetPosition = playerEntity.playerTargetPosition.position;
            playerRigidbody.DOJump(new UnityEngine.Vector2(targetPosition.x, targetPosition.y), 2f, 1, 0.5f, false).SetEase(Ease.OutSine).SetAutoKill();
            playerEntity.grounded.Grounded = false;

            entity.isDestroyed = true;
        }
    }
}