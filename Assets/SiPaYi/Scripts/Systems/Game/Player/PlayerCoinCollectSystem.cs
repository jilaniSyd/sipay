﻿using Entitas;
using System.Collections.Generic;

public class PlayerCoinCollectSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerCoinCollectSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.CoinCollected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.isPlayer)
            {
                var playerEntity = gameContext.playerEntity;
                playerEntity.coins.Amount++;

                var coinsUpdateEntity = gameContext.CreateEntity();
                coinsUpdateEntity.AddCoinUpdate(playerEntity.coins.Amount);

                SaveScore(playerEntity.coins.Amount);
            }
            entity.isDestroyed = true;
        }
    }

    private void SaveScore(int amount)
    {
        var entity = gameContext.playerCoinsEntity.playerCoins;
        entity.Amount = amount;
    }
}