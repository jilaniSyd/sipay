﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInitializeSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public PlayerInitializeSystem(Contexts contexts)
    {
        gameContext = contexts.game;
    }
    
    private CharactersData GetPlayerData(int characterId)
    {
        return Contexts.sharedInstance.game.characterBalancingData.Data[characterId];
    }

    private string GetPlayerNameById(int characterSelectedId)
    {
        var charactersData = GetPlayerData(characterSelectedId);
        var characterDataName = charactersData.Name.Replace(" ", "");
        return characterDataName;
    }

    private void CreatePlayerEntity(GameObject player, CharactersData Data)
    {
        player.transform.position = new Vector3(0, 4, 0);
        var controller = player.GetComponent<PlayerController>();
        var playerEntity = Contexts.sharedInstance.game.CreateEntity();
        playerEntity.isPlayer = true;
        playerEntity.AddPlayerController(controller);
        playerEntity.AddLives(Data.Lives);
        playerEntity.AddCoins(0);
        playerEntity.AddGrounded(false);
        playerEntity.AddRigidbody2D(controller.GetComponentInChildren<Rigidbody2D>());
        playerEntity.AddTransform(controller.GetComponent<Transform>());
        playerEntity.AddPlayerTargetPosition(Vector3.zero);
    }

    public void Initialize()
    {
        var characterSelectedId = Contexts.sharedInstance.game.characterSelectedId.Id;
        var id = GetPlayerNameById(characterSelectedId);
        var resource = Resources.Load("Prefabs/Player/" + id) as GameObject;
        var player = GameObject.Instantiate<GameObject>(resource);
        var playerData = GetPlayerData(characterSelectedId);
        CreatePlayerEntity(player, playerData);
    }
}