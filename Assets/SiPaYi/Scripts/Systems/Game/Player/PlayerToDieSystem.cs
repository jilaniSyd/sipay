﻿using Entitas;
using System.Collections.Generic;
using KnightHawkStudios.Sipayi;

public class PlayerToDieSystem : ReactiveSystem<GameEntity>
{
    public PlayerToDieSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AboutToDie);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isAboutToDie;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.transform.transform.gameObject.SetActive(false);
            ScreenHelper.ShowPopup(typeof(GameOverPopup));
        }
    }
}