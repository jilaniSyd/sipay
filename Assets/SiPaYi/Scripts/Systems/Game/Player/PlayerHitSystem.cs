﻿using Entitas;
using System.Collections.Generic;

public class PlayerGotHitSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerGotHitSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.HealthPortionLost);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.isPlayer)
            {
                var playerEntity = gameContext.playerEntity;
                if (playerEntity.lives.Amount > 0)
                {
                    playerEntity.lives.Amount--;
                    var healthUpdateEntity = gameContext.CreateEntity();
                    healthUpdateEntity.AddHeartUpdate(playerEntity.lives.Amount);
                }
                if (playerEntity.lives.Amount == 0)
                {
                    playerEntity.isAboutToDie = true;
                }
                UnityEngine.Debug.Log("Lives amount: " + playerEntity.lives.Amount);
            }

            entity.isDestroyed = true;
        }
    }
}