﻿using Entitas;
using System.Collections.Generic;

public class PlayerInBlockSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerInBlockSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.BlockStartReached);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (gameContext.isLevelData)
            {
                UnityEngine.Debug.Log("PlayerInBlock");
                var currentLevelBlock = entity.blockStartReached.levelBlock;
                var levelDataEntity = gameContext.levelDataEntity;
                var blocks = levelDataEntity.levelBlocks.Blocks;
                var indexOfCurrentBlock = blocks.FindIndex(x => x == currentLevelBlock) + 1;
                var nextLevelBlock = blocks[indexOfCurrentBlock];

                var playerEntity = gameContext.playerEntity;
                playerEntity.ReplacePlayerInBlock(entity.blockStartReached.levelBlock);
                playerEntity.ReplacePlayerNextBlock(nextLevelBlock);
                playerEntity.ReplacePlayerInBlockIndex(1);
            }
            entity.isDestroyed = true;
        }
    }
}