﻿public class PlayerSystems : Feature
{
    public PlayerSystems(Contexts contexts)
    {
        Add(new PlayerInitializeSystem(contexts));
        Add(new PlayerJumpSystem(contexts));
        Add(new PlayerAttackSystem(contexts));
        Add(new PlayerDefendSystem(contexts));
        Add(new PlayerGotHitSystem(contexts));
        Add(new PlayerCoinCollectSystem(contexts));
        Add(new PlayerHealthPortionCollectSystem(contexts));
        Add(new PlayerGroundSystem(contexts));
        Add(new PlayerInBlockSystem(contexts));
        Add(new PlayerFindNextTargetSystem(contexts));
        Add(new PlayerToDieSystem(contexts));
        Add(new PlayerDieSystem(contexts));
    }
}