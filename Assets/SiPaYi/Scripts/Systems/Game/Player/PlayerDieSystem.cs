﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System.Collections.Generic;

public class PlayerDieSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext gameContext;

    public PlayerDieSystem(Contexts contexts) : base(contexts.game)
    {
        gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Dead);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isPlayer;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            UnityEngine.Object.DestroyImmediate(entity.transform.transform.gameObject);

            entity.isDestroyed = true;
        }
    }
}