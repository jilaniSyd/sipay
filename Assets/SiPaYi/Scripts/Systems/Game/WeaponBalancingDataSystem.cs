﻿using Entitas;
using KnightHawkStudios.Sipayi;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBalancingDataSystem : IInitializeSystem
{
    private readonly GameContext gameContext;

    public WeaponBalancingDataSystem(Contexts context)
    {
        gameContext = context.game;
    }

    public void Initialize()
    {
        if (gameContext.hasWeaponsBalancingData) { return; }

        var data = GameObject.Instantiate(Resources.Load(ResourceConstants.WEAPONS_SO_PATH)) as Weapons;

        var dictionary = new Dictionary<int, WeaponsData>();
        foreach (var weaponData in data.dataArray)
        {
            var id = Convert.ToInt32(weaponData.Id);
            dictionary.Add(id, weaponData);
        }

        gameContext.ReplaceWeaponsBalancingData(dictionary);
    }
}