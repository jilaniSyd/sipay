﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class InputProcessSystem : ReactiveSystem<InputEntity>
{
    public InputProcessSystem(Contexts contexts) : base(contexts.input)
    {
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.Input);
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.isPlayerAttack || entity.isPlayerDefend || entity.isPlayerJump;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        foreach (var e in entities)
        {
            if (e.isPlayerAttack)
            {
                Contexts.sharedInstance.game.CreateEntity().isPlayerAttack = true;
            }
            if (e.isPlayerDefend)
            {
                Contexts.sharedInstance.game.CreateEntity().isPlayerDefend = true;
            }
            if (e.isPlayerJump)
            {
                Contexts.sharedInstance.game.CreateEntity().isPlayerJump = true;
            }
            e.isDestroyed = true;
        }
    }
}