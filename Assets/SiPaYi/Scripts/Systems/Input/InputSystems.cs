﻿public sealed class InputSystems : Feature
{
    public InputSystems(Contexts contexts) : base("Input Systems")
    {
        Add(new InputProcessSystem(contexts));
        Add(new InputDestroySystem(contexts));
    }
}