﻿using Entitas;
using System.Collections.Generic;

public class InputDestroySystem : ReactiveSystem<InputEntity>
{
    public InputDestroySystem(Contexts contexts) : base(contexts.input)
    {
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.Destroyed);
    }

    protected override bool Filter(InputEntity entity)
    {
        return true;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.Destroy();
        }
    }
}