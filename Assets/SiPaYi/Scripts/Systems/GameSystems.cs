public sealed class GameSystems : Feature
{
    public GameSystems(Contexts contexts)
    {
        Add(new GameInitializeSystem(contexts));
        Add(new CharacterBalancingDataSystem(contexts));
        Add(new WeaponBalancingDataSystem(contexts));
        Add(new SaveDataInitSystem(contexts));
        Add(new SaveSystems(contexts));
        Add(new ScreenChangerSystem(contexts));
        Add(new ScreenCloseSystem(contexts));
        Add(new PopupShowSystem(contexts));
        Add(new CharacterSelectSystem(contexts));
        Add(new WeaponSelectSystem(contexts));
        Add(new GameEventSystems(contexts));
        Add(new GameDestroySystem(contexts));
        Add(new GameRestartSystem(contexts));
    }
}