﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEditor;
    using UnityEditor.SceneManagement;
    using UnityEngine;

    public class LevelEditorWindow : EditorWindow
    {
        private float brushSize;
        private float elementDistance;
        private bool isSceneOpen = false;

        [MenuItem("Sipayi/LevelEditor")]
        private static void Init()
        {
            // Get existing open window or if none, make a new one:
            LevelEditorWindow window = (LevelEditorWindow)EditorWindow.GetWindow(typeof(LevelEditorWindow), false, "Level Editor");
            window.Show();
        }

        private void OnGUI()
        {
            Debug.Log("OnGUI");
            GUILayout.Label("Settings", EditorStyles.boldLabel);
            brushSize = EditorGUILayout.Slider("Brush Size", brushSize, 1f, 10f);
            LoadLevel();
            CreateButtons();
        }

        private void CreateButtons()
        {
            if (!isSceneOpen) { return; }
            GameObject[] objects = Resources.LoadAll<GameObject>("Prefabs/Obstacles/");
            if (objects.Length > 0)
            {
                EditorGUILayout.BeginHorizontal();
                for (int i = 0; i < objects.Length; i++)
                {
                    var obstacleName = objects[i].name;
                    var icon = AssetPreview.GetAssetPreview(objects[i]);
                    if (GUILayout.Button(icon, GUILayout.Width(50), GUILayout.Height(50)))
                    {
                        IconSelected(obstacleName);
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void IconSelected(string index)
        {
            Debug.Log(index);
        }

        private void LoadLevel()
        {
            isSceneOpen = IsEditorSceneOpen();
            if (isSceneOpen) { return; }

            if (GUILayout.Button("Load Level Editor Scene"))
            {
                if (!isSceneOpen)
                {
                    EditorSceneManager.OpenScene("Assets/SiPaYi/Scenes/LevelEditor.unity");
                }
            }
        }

        private bool IsEditorSceneOpen()
        {
            var scene = EditorSceneManager.GetActiveScene();
            if (scene.name == "LevelEditor" && scene.isLoaded)
            {
                return true;
            }
            return false;
        }
    }
}