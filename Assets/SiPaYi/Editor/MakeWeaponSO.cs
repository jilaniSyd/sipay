﻿namespace KnightHawkStudios.Sipayi
{
    using UnityEngine;
    using System.Collections;
    using UnityEditor;

    public class MakeWeaponSO
    {
        [MenuItem("Assets/Create/SiPaYi/SO/WeaponsSO")]
        public static void CreateMyAsset()
        {
            var asset = ScriptableObject.CreateInstance<WeaponItemList>();

            AssetDatabase.CreateAsset(asset, "Assets/SiPaYi/Resources/SO/WeaponsSO.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }
    }
}
