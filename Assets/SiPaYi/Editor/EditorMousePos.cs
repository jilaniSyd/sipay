﻿using UnityEditor;
using UnityEngine;

public class EditorMousePos : MonoBehaviour
{
    public Vector3 editorMousePos;
    public Vector3 editorMouseWorldPos;
}

[InitializeOnLoad]
public class EditorMousePosEditor
{
    static EditorMousePosEditor()
    {
        //SceneView.onSceneGUIDelegate += SceneGUI;
    }

    private static bool isDown = false;

    private static void SceneGUI(SceneView sceneView)
    {
        //            This will have scene events including mouse down on scenes objects
        // Event cur = Event.current;

        // Vector3 mousePos = (Vector3)cur.mousePosition;
        // mousePos.y = Camera.current.pixelHeight - mousePos.y;
        // Vector3 worldPos = sceneView.camera.ScreenToWorldPoint(mousePos);

        // EditorMousePos editorMoseuPos = GameObject.FindGameObjectWithTag("EditorMousePos").GetComponent<EditorMousePos>();
        // editorMoseuPos.editorMousePos = mousePos;
        // editorMoseuPos.editorMouseWorldPos = worldPos;

        Event e = Event.current;

        var controlID = GUIUtility.GetControlID(FocusType.Passive);
        var eventType = e.GetTypeForControl(controlID);
        if (eventType == EventType.MouseUp)
        {
            Debug.Log("Mouse Up!");
            GUIUtility.hotControl = controlID;
            e.Use();
        }
        else if (eventType == EventType.MouseDrag)
        {
            Debug.Log("Mouse Drag!");
            e.Use();
        }
        else if (eventType == EventType.MouseDown)
        {
            Debug.Log("Mouse Down!");
            GUIUtility.hotControl = 0;
            e.Use();
        }
    }
}