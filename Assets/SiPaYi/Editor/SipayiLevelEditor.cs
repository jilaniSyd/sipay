﻿namespace KnightHawkStudios.Sipayi
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public class SipayiLevelEditor : EditorWindow
    {
        private const string levelDataPath = "Assets/SiPaYi/Resources/LevelData/";
        private const string levelSortedDataPath = "Assets/SiPaYi/Resources/SO/LevelSortedData.asset";

        private static Dictionary<string, bool> blockSelectedDict = new Dictionary<string, bool>();

        [MenuItem("Sipayi/LevelCreator")]
        private static void Init()
        {
            // Get existing open window or if none, make a new one:
            SipayiLevelEditor window = (SipayiLevelEditor)EditorWindow.GetWindow(typeof(SipayiLevelEditor), false, "Sipayi Level Editor");
            window.Show();
        }

        private void OnGUI()
        {
            ShowButtons();
        }

        private void ShowButtons()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Level Blocks", EditorStyles.boldLabel);
            ShowLevelBlocks();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            if (GUILayout.Button("Load"))
            {
                LoadBlocks();
            }

            if (GUILayout.Button("Save"))
            {
                SaveBlocks();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void LoadBlocks()
        {
            LoadSelectedBlocks();
        }

        private void LoadSelectedBlocks()
        {
            var go = GameObject.Find("Level");
            if (go == null)
            {
                Debug.LogError("NO GO");
                return;
            }

            ClearChildren(go.transform);
            foreach (string s in blockSelectedDict.Keys)
            {
                if (blockSelectedDict[s])
                {
                    var loadedBlock = AssetDatabase.LoadAssetAtPath(s, typeof(GameObject)) as GameObject;
                    Object.Instantiate(loadedBlock, go.transform);
                }
            }
        }

        private void ClearChildren(Transform t)
        {
            var list = new List<Transform>();
            foreach (Transform child in t)
            {
                list.Add(child);
            }
            for (int i = 0; i < list.Count; i++)
            {
                DestroyImmediate(list[i].gameObject);
            }
        }

        private void SaveBlocks()
        {
            Debug.Log("Save");

            var go = GameObject.Find("Level");
            if (go == null)
            {
                Debug.LogError("NO GO");
                return;
            }
            else
            {
                foreach (Transform t in go.transform)
                {
                    var levelBlock = t.GetComponent<LevelBlock>();
                    if (levelBlock != null)
                    {
                        AddLevelEndCollider(levelBlock);
                        AddLevelStartCollider(levelBlock);
                        CreatePrefab(levelBlock);
                    }
                }
                CreateLevelSortedDataSO.CreateMyAsset();
            }
        }

        private void AddLevelStartCollider(LevelBlock levelBlock)
        {
            var levelStartBlock = levelBlock.transform.Find("LevelStart");
            if (levelStartBlock == null)
            {
                var newGo = new GameObject("LevelStart");
                newGo.AddComponent<LevelBlockStart>();
                newGo.transform.parent = levelBlock.transform;
                newGo.transform.localPosition = Vector3.zero;
                newGo.tag = Constants.TAG_LEVEL_BLOCK_START;
                var collider = newGo.AddComponent<BoxCollider2D>();
                collider.size = new Vector2(1, 10);
                collider.isTrigger = true;
            }
            else
            {
                levelStartBlock.localPosition = Vector3.zero;
                levelStartBlock.tag = Constants.TAG_LEVEL_BLOCK_START;
                levelStartBlock.GetComponent<BoxCollider2D>().isTrigger = true;
                if (levelStartBlock.GetComponent<LevelBlockStart>() == null)
                {
                    levelStartBlock.gameObject.AddComponent<LevelBlockStart>();
                }
            }
        }

        private void AddLevelEndCollider(LevelBlock levelBlock)
        {
            var levelBlockCollider = levelBlock.transform.Find("LevelEnder");
            if (levelBlockCollider == null)
            {
                var newGO = new GameObject("LevelEnder");
                var collider = newGO.AddComponent<BoxCollider2D>();
                newGO.AddComponent<LevelBlockEnd>();
                collider.size = new Vector2(1, 10);
                collider.isTrigger = true;
                newGO.transform.parent = levelBlock.transform;
                newGO.transform.localPosition = new Vector3(levelBlock.Length - 1, 0f, 0f);
                newGO.tag = Constants.TAG_LEVEL_BLOCK_END;
            }
            else
            {
                levelBlockCollider.localPosition = new Vector3(levelBlock.Length - 1, 0f, 0f);
                levelBlockCollider.tag = Constants.TAG_LEVEL_BLOCK_END;
                levelBlockCollider.GetComponent<BoxCollider2D>().isTrigger = true;
                if (levelBlockCollider.GetComponent<LevelBlockEnd>() == null)
                {
                    levelBlockCollider.gameObject.AddComponent<LevelBlockEnd>();
                }
            }
        }

        private void CreatePrefab(LevelBlock go)
        {
            //Set the path as within the Assets folder, and name it as the GameObject's name with the .prefab format
            string localPath = levelDataPath + go.BlockName + ".prefab";

            //Check if the Prefab and/or name already exists at the path
            if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
            {
                //Create dialog to ask if User is sure they want to overwrite existing Prefab
                if (EditorUtility.DisplayDialog("Are you sure?",
                    "The Prefab already exists. Do you want to overwrite it?",
                    "Yes",
                    "No"))
                //If the user presses the yes button, create the Prefab
                {
                    CreateNew(go, localPath);
                }
            }
            //If the name doesn't exist, create the new Prefab
            else
            {
                Debug.Log(go.BlockName + " is not a Prefab, will convert");
                CreateNew(go, localPath);
            }
        }

        private static void CreateNew(LevelBlock obj, string localPath)
        {
            //Create a new Prefab at the path given
            bool isSuccess = false;
            obj.Length = GetLengthOfBlock(obj.transform);
            Object prefab = PrefabUtility.SaveAsPrefabAsset(obj.gameObject, localPath, out isSuccess);
        }

        private string[] GetLevelBlocks()
        {
            return Directory.GetFiles(levelDataPath).Where(name => !name.EndsWith(".meta")).ToArray();
        }

        private void ShowLevelBlocks()
        {
            var levelBlocks = GetLevelBlocks();
            foreach (string block in levelBlocks)
            {
                var fileName = Path.GetFileNameWithoutExtension(block);
                if (blockSelectedDict.ContainsKey(block))
                {
                    blockSelectedDict[block] = EditorGUILayout.Toggle(fileName, blockSelectedDict[block]);
                }
                else
                {
                    blockSelectedDict.Add(block, false);
                    blockSelectedDict[block] = EditorGUILayout.Toggle(fileName, blockSelectedDict[block]);
                }
            }
        }

        private static float GetLengthOfBlock(Transform block)
        {
            var levelBlock = block.GetComponent<LevelBlock>();
            GroundObstacle[] groundBlocks = block.GetComponentsInChildren<GroundObstacle>(true);
            var groundBlockObstacles = new List<Obstacle>();
            groundBlockObstacles.AddRange(groundBlocks);
            levelBlock.obstacles = groundBlockObstacles;
            return groundBlocks.Length;
        }
    }
}