namespace KnightHawkStudios.Sipayi
{
    using UnityEditor;
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;

    public class CreateLevelSortedDataSO
    {
        [MenuItem("Assets/Create/SiPaYi/LevelSortedData", false, 1)]
        public static void CreateMyAsset()
        {
            var asset = ScriptableObject.CreateInstance<LevelSortedData>();
            asset = GetSortedData(asset);
            AssetDatabase.CreateAsset(asset, "Assets/SiPaYi/Resources/SO/LevelSortedData.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }

        private static LevelSortedData GetSortedData(LevelSortedData levelSortedData)
        {
            var allLevelsBlocks = Resources.LoadAll(ResourceConstants.BLOCKS_PATH, typeof(LevelBlock))
                .Cast<LevelBlock>().ToArray();
            var data = levelSortedData.Data;
            foreach (var block in allLevelsBlocks)
            {
                GetLevelBlocksList(levelSortedData, block);
            }

            return levelSortedData;
        }

        private static void GetLevelBlocksList(LevelSortedData levelSortedData, LevelBlock levelBlock)
        {
            LevelSortedInfo info = null;
            var levelInfo = levelSortedData.Data.Find(x => x._levelDifficulty == levelBlock.difficulty);
            if (levelInfo == null)
            {
                levelSortedData.Data.Add(new LevelSortedInfo(levelBlock.difficulty, new List<LevelBlock>() { levelBlock }));
            }
            else
            {
                levelInfo._levelCollection.Add(levelBlock);
            }
        }
    }
}