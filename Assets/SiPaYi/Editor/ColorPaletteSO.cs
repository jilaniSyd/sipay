namespace KnightHawkStudios.Sipayi
{
    using UnityEditor;
    using UnityEngine;

    public class ColorPaletteSO
    {
        [MenuItem("Assets/Create/SiPaYi/SO/ColorPaletteSO")]
        public static void CreateMyAsset()
        {
            var asset = ScriptableObject.CreateInstance<ColorPalettes>();

            AssetDatabase.CreateAsset(asset, "Assets/SiPaYi/Resources/SO/ColorPalettes/ColorPaletteSO.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }
    }
}