//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity playerEpicScoreEntity { get { return GetGroup(GameMatcher.PlayerEpicScore).GetSingleEntity(); } }
    public PlayerEpicScoreComponent playerEpicScore { get { return playerEpicScoreEntity.playerEpicScore; } }
    public bool hasPlayerEpicScore { get { return playerEpicScoreEntity != null; } }

    public GameEntity SetPlayerEpicScore(int newAmount) {
        if (hasPlayerEpicScore) {
            throw new Entitas.EntitasException("Could not set PlayerEpicScore!\n" + this + " already has an entity with PlayerEpicScoreComponent!",
                "You should check if the context already has a playerEpicScoreEntity before setting it or use context.ReplacePlayerEpicScore().");
        }
        var entity = CreateEntity();
        entity.AddPlayerEpicScore(newAmount);
        return entity;
    }

    public void ReplacePlayerEpicScore(int newAmount) {
        var entity = playerEpicScoreEntity;
        if (entity == null) {
            entity = SetPlayerEpicScore(newAmount);
        } else {
            entity.ReplacePlayerEpicScore(newAmount);
        }
    }

    public void RemovePlayerEpicScore() {
        playerEpicScoreEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PlayerEpicScoreComponent playerEpicScore { get { return (PlayerEpicScoreComponent)GetComponent(GameComponentsLookup.PlayerEpicScore); } }
    public bool hasPlayerEpicScore { get { return HasComponent(GameComponentsLookup.PlayerEpicScore); } }

    public void AddPlayerEpicScore(int newAmount) {
        var index = GameComponentsLookup.PlayerEpicScore;
        var component = CreateComponent<PlayerEpicScoreComponent>(index);
        component.Amount = newAmount;
        AddComponent(index, component);
    }

    public void ReplacePlayerEpicScore(int newAmount) {
        var index = GameComponentsLookup.PlayerEpicScore;
        var component = CreateComponent<PlayerEpicScoreComponent>(index);
        component.Amount = newAmount;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerEpicScore() {
        RemoveComponent(GameComponentsLookup.PlayerEpicScore);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerEpicScore;

    public static Entitas.IMatcher<GameEntity> PlayerEpicScore {
        get {
            if (_matcherPlayerEpicScore == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerEpicScore);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerEpicScore = matcher;
            }

            return _matcherPlayerEpicScore;
        }
    }
}
