//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PopupShowComponent popupShow { get { return (PopupShowComponent)GetComponent(GameComponentsLookup.PopupShow); } }
    public bool hasPopupShow { get { return HasComponent(GameComponentsLookup.PopupShow); } }

    public void AddPopupShow(System.Type newPopup) {
        var index = GameComponentsLookup.PopupShow;
        var component = CreateComponent<PopupShowComponent>(index);
        component.Popup = newPopup;
        AddComponent(index, component);
    }

    public void ReplacePopupShow(System.Type newPopup) {
        var index = GameComponentsLookup.PopupShow;
        var component = CreateComponent<PopupShowComponent>(index);
        component.Popup = newPopup;
        ReplaceComponent(index, component);
    }

    public void RemovePopupShow() {
        RemoveComponent(GameComponentsLookup.PopupShow);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPopupShow;

    public static Entitas.IMatcher<GameEntity> PopupShow {
        get {
            if (_matcherPopupShow == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PopupShow);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPopupShow = matcher;
            }

            return _matcherPopupShow;
        }
    }
}
