//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly BlockEndReachedComponent blockEndReachedComponent = new BlockEndReachedComponent();

    public bool isBlockEndReached {
        get { return HasComponent(GameComponentsLookup.BlockEndReached); }
        set {
            if (value != isBlockEndReached) {
                var index = GameComponentsLookup.BlockEndReached;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : blockEndReachedComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherBlockEndReached;

    public static Entitas.IMatcher<GameEntity> BlockEndReached {
        get {
            if (_matcherBlockEndReached == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.BlockEndReached);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherBlockEndReached = matcher;
            }

            return _matcherBlockEndReached;
        }
    }
}
