//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public LevelConfigComponent levelConfig { get { return (LevelConfigComponent)GetComponent(GameComponentsLookup.LevelConfig); } }
    public bool hasLevelConfig { get { return HasComponent(GameComponentsLookup.LevelConfig); } }

    public void AddLevelConfig(KnightHawkStudios.Sipayi.LevelConfig newConfig) {
        var index = GameComponentsLookup.LevelConfig;
        var component = CreateComponent<LevelConfigComponent>(index);
        component.Config = newConfig;
        AddComponent(index, component);
    }

    public void ReplaceLevelConfig(KnightHawkStudios.Sipayi.LevelConfig newConfig) {
        var index = GameComponentsLookup.LevelConfig;
        var component = CreateComponent<LevelConfigComponent>(index);
        component.Config = newConfig;
        ReplaceComponent(index, component);
    }

    public void RemoveLevelConfig() {
        RemoveComponent(GameComponentsLookup.LevelConfig);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherLevelConfig;

    public static Entitas.IMatcher<GameEntity> LevelConfig {
        get {
            if (_matcherLevelConfig == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.LevelConfig);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherLevelConfig = matcher;
            }

            return _matcherLevelConfig;
        }
    }
}
