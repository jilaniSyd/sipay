using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Weapons))]
public class WeaponsEditor : BaseGoogleEditor<Weapons>
{	    
    public override bool Load()
    {        
        Weapons targetData = target as Weapons;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<WeaponsData>(targetData.WorksheetName) ?? db.CreateTable<WeaponsData>(targetData.WorksheetName);
        
        List<WeaponsData> myDataList = new List<WeaponsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            WeaponsData data = new WeaponsData();
            
            data = Cloner.DeepCopy<WeaponsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
