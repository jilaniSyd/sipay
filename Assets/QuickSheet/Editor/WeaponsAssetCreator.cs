using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Weapons")]
    public static void CreateWeaponsAssetFile()
    {
        Weapons asset = CustomAssetUtility.CreateAsset<Weapons>();
        asset.SheetName = "BalancingData";
        asset.WorksheetName = "Weapons";
        EditorUtility.SetDirty(asset);        
    }
    
}