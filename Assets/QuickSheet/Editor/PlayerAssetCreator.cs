using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Player")]
    public static void CreatePlayerAssetFile()
    {
        Player asset = CustomAssetUtility.CreateAsset<Player>();
        asset.SheetName = "BalancingData";
        asset.WorksheetName = "Player";
        EditorUtility.SetDirty(asset);        
    }
    
}