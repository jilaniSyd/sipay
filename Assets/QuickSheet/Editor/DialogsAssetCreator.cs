using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Dialogs")]
    public static void CreateDialogsAssetFile()
    {
        Dialogs asset = CustomAssetUtility.CreateAsset<Dialogs>();
        asset.SheetName = "BalancingData";
        asset.WorksheetName = "Dialogs";
        EditorUtility.SetDirty(asset);        
    }
    
}