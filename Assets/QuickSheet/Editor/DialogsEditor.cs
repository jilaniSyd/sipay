using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Dialogs))]
public class DialogsEditor : BaseGoogleEditor<Dialogs>
{	    
    public override bool Load()
    {        
        Dialogs targetData = target as Dialogs;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<DialogsData>(targetData.WorksheetName) ?? db.CreateTable<DialogsData>(targetData.WorksheetName);
        
        List<DialogsData> myDataList = new List<DialogsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            DialogsData data = new DialogsData();
            
            data = Cloner.DeepCopy<DialogsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
