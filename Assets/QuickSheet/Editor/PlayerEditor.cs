using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Player))]
public class PlayerEditor : BaseGoogleEditor<Player>
{	    
    public override bool Load()
    {        
        Player targetData = target as Player;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<PlayerData>(targetData.WorksheetName) ?? db.CreateTable<PlayerData>(targetData.WorksheetName);
        
        List<PlayerData> myDataList = new List<PlayerData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            PlayerData data = new PlayerData();
            
            data = Cloner.DeepCopy<PlayerData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
