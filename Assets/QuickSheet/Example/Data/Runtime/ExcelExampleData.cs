using UnityEngine;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class ExcelExampleData
{
    [SerializeField]
    private int id;

    public int Id { get { return id; } set { id = value; } }

    [SerializeField]
    private string name;

    public string Name { get { return name; } set { name = value; } }

    [SerializeField]
    private float strength;

    public float Strength { get { return strength; } set { strength = value; } }

    [SerializeField]
    private Difficulty difficulty;

    public Difficulty DIFFICULTY { get { return difficulty; } set { difficulty = value; } }
}