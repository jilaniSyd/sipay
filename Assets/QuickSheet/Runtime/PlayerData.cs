using UnityEngine;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class PlayerData
{
    [SerializeField]
    private string name;

    public string Name { get { return name; } set { name = value; } }

    [SerializeField]
    private int id;

    public int Id { get { return id; } set { id = value; } }

    [SerializeField]
    private int health;

    public int Health { get { return health; } set { health = value; } }

    [SerializeField]
    private int stamina;

    public int Stamina { get { return stamina; } set { stamina = value; } }

    [SerializeField]
    private int damage;

    public int Damage { get { return damage; } set { damage = value; } }
}