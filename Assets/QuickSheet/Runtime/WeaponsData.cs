using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class WeaponsData
{
  [SerializeField]
  string name;
  public string Name { get {return name; } set { name = value;} }
  
  [SerializeField]
  string id;
  public string Id { get {return id; } set { id = value;} }
  
  [SerializeField]
  int damage;
  public int Damage { get {return damage; } set { damage = value;} }
  
}