using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class DialogsData
{
  [SerializeField]
  string dialogid;
  public string Dialogid { get {return dialogid; } set { dialogid = value;} }
  
  [SerializeField]
  CharacterType charactertype;
  public CharacterType CHARACTERTYPE { get {return charactertype; } set { charactertype = value;} }
  
  [SerializeField]
  string dialog;
  public string Dialog { get {return dialog; } set { dialog = value;} }
  
}